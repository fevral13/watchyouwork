"""watchyouwork URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from functools import partial

from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth.views import logout
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from main.views.api.change_password import ChangePasswordAPI
from main.views.api.contact import ContactListAPI, ContactDetailAPI
from main.views.api.monitor import MonitorCreateListAPI, MonitorUpdateAPI
from main.views.api.monitor_data import MonitorDataAPI
from main.views.api.monitor_event import MonitorEventsAPI
from main.views.api.monitor_status import MonitorStatusAPI
from main.views.api.profile import ProfileAPI
from main.views.api.region import RegionAPI
from main.views.api.timezones import TimezonesAPI
from main.views.api.verify_phone import VerifyPhoneAPI, InitVerifyPhoneAPI


urlpatterns = [
    url(r'^$', 'main.views.index.index', name='index'),
    url(r'^accounts/', include('allauth.urls')),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^dashboard/$', 'main.views.dashboard.dashboard'),
    # url(r'^logout/$', partial(logout, next_page='/'), name='logout'),
    url(r'^verify_email/(?P<verification_token>.+)/$', 'main.views.verify_email.verify_email', name='verify_email'),
    url(r'^reset_password_confirm/(?P<reset_token>.+)/$',
        'main.views.reset_password_confirm.reset_password_confirm', name='reset_password_confirm'),

    url(r'^api/auth/registration/$', 'main.views.auth.registration.api_registration'),
    url(r'^api/auth/login/$', 'main.views.auth.login.api_login'),
    url(r'^api/auth/reset_password/$', 'main.views.auth.reset_password.api_reset_password'),

    url(r'^api/monitor/$', MonitorCreateListAPI.as_view()),
    url(r'^api/monitor/(?P<pk>\d+)/$', MonitorUpdateAPI.as_view()),
    url(r'^api/monitor_data/(?P<monitor_id>\d+)/$', MonitorDataAPI.as_view()),
    url(r'^api/monitor_events/$', MonitorEventsAPI.as_view()),
    url(r'^api/monitor_statuses/$', MonitorStatusAPI.as_view()),
    url(r'^api/profile/$', ProfileAPI.as_view()),
    url(r'^api/timezones/$', TimezonesAPI.as_view()),

    url(r'^api/contact/$', ContactListAPI.as_view()),
    url(r'^api/contact/(?P<pk>[0-9]+)/$', ContactDetailAPI.as_view()),
    url(r'^api/change_password/$', ChangePasswordAPI.as_view()),
    url(r'^api/init_verify_phone/$', InitVerifyPhoneAPI.as_view()),
    url(r'^api/verify_phone/$', VerifyPhoneAPI.as_view()),
    url(r'^api/region/$', RegionAPI.as_view()),

    url(r'^admin/upload_monitors/',
        'main.views.admin.upload_monitors.upload_monitors',
        name='upload_monitors'),
    url(r'^admin/', include(admin.site.urls)),
] + staticfiles_urlpatterns()
