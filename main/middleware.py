# -*- coding: utf8 -*-
import pytz
from django.conf import settings
from django.utils import timezone


class TimezoneMiddleware(object):
    def process_request(self, request):
        if request.user.is_authenticated():

            tzname = request.session.get('django_timezone')
            if not tzname:
                tzname = request.user.profile.timezone or settings.TIME_ZONE

            timezone.activate(pytz.timezone(tzname))
        else:
            timezone.deactivate()
