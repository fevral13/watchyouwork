# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20150615_1308'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitor',
            name='region',
            field=models.IntegerField(choices=[(1, b'New York'), (2, b'Amsterdam'), (3, b'Singapore')]),
        ),
        migrations.AlterField(
            model_name='monitor',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, b'Unknown'), (2, b'Up'), (4, b'Down'), (1, b'Just started'), (3, b'Paused')]),
        ),
        migrations.AlterField(
            model_name='monitordata',
            name='region',
            field=models.IntegerField(choices=[(1, b'New York'), (2, b'Amsterdam'), (3, b'Singapore')]),
        ),
        migrations.AlterField(
            model_name='monitordata',
            name='status',
            field=models.IntegerField(choices=[(0, b'Unknown'), (2, b'Up'), (4, b'Down'), (1, b'Just started'), (3, b'Paused')]),
        ),
        migrations.AlterField(
            model_name='monitorevent',
            name='type',
            field=models.IntegerField(choices=[(0, b'Unknown'), (2, b'Up'), (4, b'Down'), (1, b'Just started'), (3, b'Paused')]),
        ),
    ]
