# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0006_auto_20150615_1249'),
    ]

    operations = [
        migrations.RunSQL('''
        CREATE OR REPLACE FUNCTION monitor_data_status_grouped(m_id int, inter interval, dur interval)
          returns table (
            dates TIMESTAMPTZ, status integer, dur interval
          )
        AS $$
           select dates.d, coalesce(max(md.status), 3), coalesce(avg(md.duration), interval '0')
            from
        	 (select d FROM generate_series(date_round(current_timestamp - dur, inter),
        		      date_round(current_timestamp, inter), inter) as d
              ) dates
          left outer join
              (select duration, status, date_round(datetime_created, inter) dr from main_monitordata
        		    where monitor_id = m_id and datetime_created > current_timestamp - dur
              ) md
              on md.dr = dates.d

          group by dates.d
          order by dates.d;
        $$
        LANGUAGE SQL;''')
    ]
