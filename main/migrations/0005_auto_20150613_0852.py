# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20150613_0747'),
    ]

    operations = [
        migrations.AlterField(
            model_name='monitor',
            name='region',
            field=models.IntegerField(choices=[(1, '\u041d\u044c\u044e-\u0419\u043e\u0440\u043a'), (2, '\u0410\u043c\u0441\u0442\u0435\u0440\u0434\u0430\u043c'), (3, '\u0421\u0438\u043d\u0433\u0430\u043f\u0443\u0440')]),
        ),
        migrations.AlterField(
            model_name='monitor',
            name='status',
            field=models.IntegerField(default=0, choices=[(0, '\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u043e'), (2, '\u0420\u0430\u0431\u043e\u0442\u0430\u0435\u0442'), (4, '\u041d\u0435 \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442'), (1, '\u0417\u0430\u043f\u0443\u0449\u0435\u043d'), (3, '\u041e\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d')]),
        ),
        migrations.AlterField(
            model_name='monitordata',
            name='region',
            field=models.IntegerField(choices=[(1, '\u041d\u044c\u044e-\u0419\u043e\u0440\u043a'), (2, '\u0410\u043c\u0441\u0442\u0435\u0440\u0434\u0430\u043c'), (3, '\u0421\u0438\u043d\u0433\u0430\u043f\u0443\u0440')]),
        ),
        migrations.AlterField(
            model_name='monitordata',
            name='status',
            field=models.IntegerField(choices=[(0, '\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u043e'), (2, '\u0420\u0430\u0431\u043e\u0442\u0430\u0435\u0442'), (4, '\u041d\u0435 \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442'), (1, '\u0417\u0430\u043f\u0443\u0449\u0435\u043d'), (3, '\u041e\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d')]),
        ),
        migrations.AlterField(
            model_name='monitorevent',
            name='type',
            field=models.IntegerField(choices=[(0, '\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u043e'), (2, '\u0420\u0430\u0431\u043e\u0442\u0430\u0435\u0442'), (4, '\u041d\u0435 \u0440\u0430\u0431\u043e\u0442\u0430\u0435\u0442'), (1, '\u0417\u0430\u043f\u0443\u0449\u0435\u043d'), (3, '\u041e\u0441\u0442\u0430\u043d\u043e\u0432\u043b\u0435\u043d')]),
        ),
    ]
