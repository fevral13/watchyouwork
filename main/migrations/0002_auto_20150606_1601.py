# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL('''
                CREATE FUNCTION date_round(base_date timestamptz, round_interval INTERVAL) RETURNS timestamptz AS $BODY$
                SELECT TO_TIMESTAMP((EXTRACT(epoch FROM $1)::INTEGER + EXTRACT(epoch FROM $2)::INTEGER / 2)
                                / EXTRACT(epoch FROM $2)::INTEGER * EXTRACT(epoch FROM $2)::INTEGER)
                $BODY$ LANGUAGE SQL STABLE;
                ''',
                                  '''
                                  date_round(base_date timestamptz, round_interval INTERVAL);
                                  '''),
        migrations.RunSQL('''
                CREATE OR REPLACE FUNCTION monitor_data_grouped(m_id int, inter interval)
                  returns table (
                    dates TIMESTAMPTZ, dur interval
                  )
                AS $$
                   select dates.d, coalesce(avg(md.duration), interval '0')
                    from
                         (select d FROM generate_series(date_round(current_timestamp - interval '1 day', inter),
                                      date_round(current_timestamp, inter), inter) as d
                      ) dates
                  left outer join
                      (select duration, date_round(datetime_created, inter) dr from main_monitordata
                                    where monitor_id = m_id and datetime_created > current_timestamp - interval '1 day'
                      ) md
                      on md.dr = dates.d

                  group by dates.d
                  order by dates.d;
                $$
                LANGUAGE SQL;
                                ''')
    ]
