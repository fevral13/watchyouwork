# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def migrate(apps, schema_editor):
    MonitorData = apps.get_model('main.MonitorData')
    MonitorData.objects.filter(status=1).update(status=10)
    MonitorData.objects.filter(status=2).update(status=20)
    MonitorData.objects.filter(status=3).update(status=30)
    MonitorData.objects.filter(status=4).update(status=40)

    MonitorData.objects.filter(status=10).update(status=2)
    MonitorData.objects.filter(status=20).update(status=4)
    MonitorData.objects.filter(status=30).update(status=1)
    MonitorData.objects.filter(status=40).update(status=3)
    
    MonitorEvent = apps.get_model('main.MonitorEvent')
    MonitorEvent.objects.filter(type=1).update(type=10)
    MonitorEvent.objects.filter(type=2).update(type=20)
    MonitorEvent.objects.filter(type=3).update(type=30)
    MonitorEvent.objects.filter(type=4).update(type=40)

    MonitorEvent.objects.filter(type=10).update(type=2)
    MonitorEvent.objects.filter(type=20).update(type=4)
    MonitorEvent.objects.filter(type=30).update(type=1)
    MonitorEvent.objects.filter(type=40).update(type=3)

    Monitor = apps.get_model('main.Monitor')
    Monitor.objects.filter(status=1).update(status=10)
    Monitor.objects.filter(status=2).update(status=20)
    Monitor.objects.filter(status=3).update(status=30)
    Monitor.objects.filter(status=4).update(status=40)

    Monitor.objects.filter(status=10).update(status=2)
    Monitor.objects.filter(status=20).update(status=4)
    Monitor.objects.filter(status=30).update(status=1)
    Monitor.objects.filter(status=40).update(status=3)


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20150606_1601'),
    ]

    operations = [
        migrations.RunPython(migrate)
    ]
