# -*- coding: utf8 -*-
from django import template
from django.conf import settings
from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse

register = template.Library()


@register.simple_tag
def absolute_url(name, *args, **kwargs):
    path = reverse(name, args=args, kwargs=kwargs)
    return '{protocol}://{domain}{path}'.format(
        protocol=settings.PROTOCOL,
        domain=Site.objects.get_current().domain,
        path=path
    )
