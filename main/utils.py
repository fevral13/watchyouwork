# -*- coding: utf8 -*-
import logging
import os
from collections import OrderedDict
from contextlib import contextmanager
from functools import wraps

import time
from datetime import datetime
from six.moves.urllib_parse import urlsplit

import re
from django.conf import settings
from django.core.mail import mail_admins
from rest_framework import pagination
from rest_framework.response import Response

logger = logging.getLogger(__name__)


def be_unique(func):
    @contextmanager
    def _pidfile(filename):
        with open(filename, 'w') as f:
            f.write(str(datetime.now()))
        try:
            yield
        finally:
            os.remove(filename)

    @wraps(func)
    def wrapper(monitor_id, *args, **kwargs):
        filename = os.path.join('/tmp', u'%s.%s-%s.pid' % (func.__module__, func.__name__, monitor_id))
        if not os.path.isfile(filename):
            with _pidfile(filename):
                return func(monitor_id, *args, **kwargs)
        elif not settings.DEBUG:
            file_time_seconds = os.path.getctime(filename)
            context = {'module': func.__module__,
                       'func_name': func.__name__,
                       'ftime': (time.asctime(time.localtime(file_time_seconds))),
                       'ntime': (datetime.now().ctime()),
                       'monitor_id': monitor_id,
                       }
            mail_admins(u'{module}.{func_name} skipped to run'.format(**context),
                        u'Previous instance of task {module}.{func_name} (monitor_id={monitor_id}) found still running. Pidfile creation time {ftime}, now is {ntime}'.format(**context))

        logger.debug(u'Pidfile found, exiting')

    return wrapper


class LegacyPaginationClass(pagination.PageNumberPagination):
    """
    Uses old response field names to meet frontend's expectations
    """
    page_size = 10

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('totalRecords', self.page.paginator.count),
            ('page', self.page.number),
            ('objects', data),
        ]))


# special characters appearing in URLs
_non_domain_special_chars = re.compile('//|:|\?|&')


def get_domain_from_url(url):
    """
    Get only domain part from url if it looks like "http://domain.com"

    :param url:
    :return: domain name
    """
    if _non_domain_special_chars.search(url):
        return urlsplit(url).netloc
    else:
        return url
