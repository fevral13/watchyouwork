# -*- coding: utf8 -*-
from celery.task import task
from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string


@task(ignore_result=True)
def send_email(to, subject, text_template, html_template, context):
    if not isinstance(to, (tuple, list)):
        to = [to]

    kwargs = dict(
        subject=settings.EMAIL_SUBJECT_PREFIX + subject,
        message=render_to_string(text_template, context),
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=to,
        html_message=render_to_string(html_template, context)
    )
    send_mail(**kwargs)
