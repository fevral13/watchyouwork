# -*- coding: utf8 -*-
from celery.task import task
from django.conf import settings
from django.template.loader import render_to_string
from django_twilio.client import twilio_client


@task(ignore_result=True)
def send_sms(to, text_template, context):
    text = render_to_string(text_template, context)
    twilio_client.sms.messages.create(_from=settings.FROM_SMS, to=to, body=text[:160])
