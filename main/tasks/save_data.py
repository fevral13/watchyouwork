# -*- coding:utf-8 -*-
from celery.task import task
from datetime import timedelta
from redis import ResponseError

from main import MONITOR_DATA_STATUS_DOWN, MONITOR_DATA_STATUS_UP, MONITOR_DATA_STATUS_PAUSED
from main.models import MonitorData, Monitor, MonitorEvent
from main.models.monitor_event import MonitorEventSerializer
from main.redis_messages import client
from main.redis_messages import publish_message
from main.utils import be_unique


@task(ignore_result=True)
@be_unique
def save_data(monitor_id, action_date_time, action_duration, action_status_id, response_status_code=0):
    monitor = Monitor.objects.get(pk=monitor_id)
    data_id = MonitorData.objects.create_new_record(
        monitor_id=monitor_id,
        datetime_created=action_date_time,
        status=action_status_id,
        duration=action_duration,
        region=monitor.region,
        response_status_code=response_status_code
    )
    cache_key = monitor.status_cache_key()

    try:
        last_statuses = client.lrange(cache_key, 0, 1)
    except ResponseError as e:
        client.delete(cache_key)
        last_statuses = []

    client.lpush(cache_key, action_status_id)
    from .monitor_status_change import monitor_status_change

    if not last_statuses:
        # change_status
        monitor_status_change.delay(monitor.id, data_id)

    # if was UP, but now twice DOWN
    elif action_status_id == MONITOR_DATA_STATUS_DOWN and last_statuses == [str(MONITOR_DATA_STATUS_DOWN),
                                                                            str(MONITOR_DATA_STATUS_UP)] \
            or action_status_id == MONITOR_DATA_STATUS_UP and last_statuses[0] == str(
                MONITOR_DATA_STATUS_DOWN):  # or was DOWN, but now UP
        monitor_status_change.delay(monitor.id, data_id)
    if len(last_statuses) == 2:
        client.rpop(cache_key)

    if monitor.status != action_status_id:
        monitor.status = action_status_id
        monitor.save()

        event = MonitorEvent.objects.create(
            monitor=monitor,
            type=action_status_id,
            duration=timedelta(days=0)
        )

        if monitor.status != MONITOR_DATA_STATUS_PAUSED:
            publish_message(monitor.user_id,
                            'monitor:changed',
                            MonitorEventSerializer(event).data)
