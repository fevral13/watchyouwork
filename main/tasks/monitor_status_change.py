# -*- coding:utf-8 -*-
from celery.task import task

from main.models import Monitor
from main.models import MonitorData


@task(ignore_result=True)
def monitor_status_change(monitor_id, monitor_data_id):
    monitor = Monitor.objects.get(pk=monitor_id)
    monitor_data = MonitorData.objects.get(pk=monitor_data_id)
    monitor.on_status_change(monitor_data)
