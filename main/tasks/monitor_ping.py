# -*- coding:utf-8 -*-
import os
import re
from decimal import Decimal

from celery.task import task
from datetime import timedelta
from django.utils.timezone import now

from main import MONITOR_DATA_STATUS_DOWN, MONITOR_DATA_STATUS_UP
from main.tasks.save_data import save_data

ping_time_re = re.compile(r'.*?time=([\d.]+)\sms')


@task(ignore_result=True)
def monitor_ping(monitor_id, url, kwargs):
    for _ in xrange(3):
        action_datetime = now()
        pipe = os.popen('ping -c 1 %s' % url)
        try:
            response = pipe.readlines()[1]
            ping_time_string = ping_time_re.findall(response)[0]
        except IndexError:
            status = MONITOR_DATA_STATUS_DOWN
            duration = timedelta(days=0)
        else:
            status = MONITOR_DATA_STATUS_UP
            duration = timedelta(milliseconds=int(Decimal(ping_time_string) * 1000))
            break

    save_data.delay(
        monitor_id=monitor_id,
        action_date_time=action_datetime,
        action_duration=duration,
        action_status_id=status
    )
