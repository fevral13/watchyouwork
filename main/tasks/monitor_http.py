# -*- coding:utf-8 -*-
import requests
import requests.exceptions
from celery.task import task
from datetime import timedelta
from django.utils.timezone import now
from requests.auth import HTTPBasicAuth

from main import MONITOR_DATA_STATUS_DOWN, MONITOR_DATA_STATUS_UP, DEFAULT_TIMEOUT
from main.tasks.save_data import save_data


@task(ignore_result=True)
def monitor_http(monitor_id, url, kwargs):
    action_datetime = now()
    verb = kwargs.get('verb', 'GET')
    username = kwargs.get('username', '')
    password = kwargs.get('password', '')
    keyword = kwargs.get('keyword', '')
    should = kwargs.get('should')
    duration = timedelta(days=0)
    response_status_code = 0

    auth = username and HTTPBasicAuth(username, password) or None

    for _ in xrange(3):
        try:
            methods = {
                'HEAD': requests.head,
                'GET': requests.get,
            }
            response = methods[verb](url, auth=auth, timeout=DEFAULT_TIMEOUT, verify=False)
        except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout):
            status = MONITOR_DATA_STATUS_DOWN
        else:

            duration = timedelta(microseconds=response.elapsed.microseconds, seconds=response.elapsed.total_seconds())
            response_status_code = response.status_code

            if keyword and verb == 'GET':
                keyword_in_response = keyword in response.text
                response_status_code = response.status_code
                if response_status_code == 200 and ((should and keyword_in_response) or (not should and not keyword_in_response)):
                    status = MONITOR_DATA_STATUS_UP
                    break
                else:
                    status = MONITOR_DATA_STATUS_DOWN
            else:
                if response_status_code in (200, 301, 302):
                    status = MONITOR_DATA_STATUS_UP
                    break
                else:
                    status = MONITOR_DATA_STATUS_DOWN

    save_data.delay(
        monitor_id=monitor_id,
        action_date_time=action_datetime,
        action_duration=duration,
        action_status_id=status,
        response_status_code=response_status_code
    )
