# -*- coding:utf-8 -*-
from django import forms

from main.forms.monitor_ping import MonitorPingForm

VERB_CHOICES = (
    ('HEAD', 'HEAD'),
    ('GET', 'GET'),
)

SHOULD_BE = 1
SHOULD_NOT_BE = 0

SHOULD_CHOICES = (
    (SHOULD_BE, 'Must be'),
    (SHOULD_NOT_BE, 'Must NOT be'),
)


class MonitorHTTPForm(MonitorPingForm):
    verb = forms.ChoiceField(choices=VERB_CHOICES)
    keyword = forms.CharField(required=False)
    should = forms.ChoiceField(choices=SHOULD_CHOICES, required=False)

    username = forms.CharField(required=False)
    password = forms.CharField(required=False)

    def clean_url(self):
        url = self.cleaned_data.get('url')
        if url and not (url.startswith('http://') or url.startswith('https://')):
            raise forms.ValidationError('Enter url with protocol: http:// or https://')

        return url

    def clean_should(self):
        should = self.cleaned_data.get('should')
        if should:
            return int(should)

    def clean(self):
        cd = self.cleaned_data

        if cd.get('keyword') and cd.get('should') is None:
            self.add_error('should', 'Required if keyword is entered')
        if cd.get('password') and not cd.get('username'):
            self.add_error('username', 'Required if password is entered')

        self.extra = {
            'verb': cd.get('verb'),
            'keyword': cd.get('keyword'),
            'should': cd.get('should'),
            'username': cd.get('username'),
            'password': cd.get('password'),
        }
        return cd
