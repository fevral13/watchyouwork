# -*- coding:utf-8 -*-
from django import forms

from main.models import Monitor


class MonitorPingForm(forms.ModelForm):
    extra = {}

    class Meta:
        model = Monitor
        fields = ('type', 'name', 'url', 'interval_schedule', 'region', 'running')
