# -*- coding:utf-8 -*-
from django import forms
from django.contrib.auth.models import User

from main.models.contact import serializer


class ResetPasswordConfirmForm(forms.Form):
    password = forms.CharField()
    token = forms.CharField(widget=forms.HiddenInput())

    def clean(self):
        cd = self.cleaned_data
        try:
            email = serializer.loads(cd.get('token'))['email']
            self.user = User.objects.get(email=email)
        except:
            raise forms.ValidationError('Wrong token')
        return cd

    def save(self):
        self.user.set_password(self.cleaned_data['password'])
        self.user.save()
