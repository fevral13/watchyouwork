# -*- coding:utf-8 -*-
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()

    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email=email).exists():
            raise forms.ValidationError('User not found')
        return email

    def clean(self):
        cd = self.cleaned_data

        if not self.errors:
            self._user = authenticate(username=cd.get('email'), password=cd.get('password'))
            if not self._user:
                self.add_error('password', 'Wrong password')
        return cd

    def save(self):
        return self._user
