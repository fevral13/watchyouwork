# -*- coding:utf-8 -*-
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User

from main import CONTACT_TYPE_EMAIL
from main.models import Contact


class RegistrationForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).exists():
            raise forms.ValidationError('This email is already taken')

        return email

    def save(self, request):
        cd = self.cleaned_data
        user = User.objects.create_user(username=cd['email'], email=cd['email'], password=cd['password'])

        # this relies on User post_save signal and expects user's Profile
        # to be created
        profile = user.profile
        ip = request.META.get('HTTP_X_FORWARDED_FOR', '')
        profile.guess_timezone(ip)
        Contact.objects.create(
            user=user,
            contact_type=CONTACT_TYPE_EMAIL,
            value=cd['email']
        )

        return authenticate(username=cd['email'], password=cd['password'])
