# -*- coding:utf-8 -*-
from django import forms
from django.contrib.auth.models import User

from main.models.contact import serializer


class ResetPasswordForm(forms.Form):
    email = forms.EmailField()

    def clean_email(self):
        cd = self.cleaned_data
        email = cd.get('email')
        try:
            User.objects.get(email=email)
        except User.DoesNotExist:
            raise forms.ValidationError('Email not found')
        return email

    def save(self):
        from main.tasks import send_email

        email = self.cleaned_data['email']
        context = {
            'token': serializer.dumps({'email': email})
        }

        send_email.delay(email, 'Password reset', 'emails/reset_password.txt', 'emails/reset_password.html', context)
