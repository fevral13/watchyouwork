# -*- coding: utf8 -*-
from django import forms
from django.contrib.auth.models import User
from djcelery.models import IntervalSchedule

from main import REGION_CHOICES, MONITOR_TYPE_HTTP
from main.models import Monitor


class UploadMonitorsForm(forms.Form):
    user = forms.ModelChoiceField(User.objects)
    region = forms.ChoiceField(REGION_CHOICES)
    interval_schedule = forms.ModelChoiceField(IntervalSchedule.objects)
    args = forms.CharField()
    csv = forms.FileField()

    def save(self):
        cd = self.cleaned_data
        for line in cd['csv'].readlines():
            Monitor.objects.create(
                user=cd['user'],
                region=cd['region'],
                url=line,
                type=MONITOR_TYPE_HTTP,
                interval_schedule=cd['interval_schedule'],
                args=cd['args']
            )
