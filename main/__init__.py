# -*- coding:utf-8 -*-
MONITOR_TYPE_PING = 1
MONITOR_TYPE_HTTP = 2

MONITOR_TYPE_CHOICES = (
    (MONITOR_TYPE_PING, u'Ping'),
    (MONITOR_TYPE_HTTP, u'HTTP(S)'),
)

MONITOR_TASKS = (
    (MONITOR_TYPE_PING, 'main.tasks.monitor_ping.monitor_ping'),
    (MONITOR_TYPE_HTTP, 'main.tasks.monitor_http.monitor_http'),
)

import pytz

TIMEZONE_CHOICES = zip(pytz.common_timezones, pytz.common_timezones)

MONITOR_DATA_STATUS_UNKNOWN = 0
MONITOR_DATA_STATUS_STARTED = 1  # was 3
MONITOR_DATA_STATUS_UP = 2  # was 1
MONITOR_DATA_STATUS_PAUSED = 3  # was 4
MONITOR_DATA_STATUS_DOWN = 4  # was 2

MONITOR_DATA_STATUS_CHOICES = (
    (MONITOR_DATA_STATUS_UNKNOWN, 'Unknown'),
    (MONITOR_DATA_STATUS_UP, 'Up'),
    (MONITOR_DATA_STATUS_DOWN, 'Down'),
    (MONITOR_DATA_STATUS_STARTED, 'Just started'),
    (MONITOR_DATA_STATUS_PAUSED, 'Paused'),
)

CONTACT_TYPE_EMAIL = 1
CONTACT_TYPE_PHONE = 2

CONTACT_TYPE_CHOICES = (
    (CONTACT_TYPE_EMAIL, 'Email'),
    (CONTACT_TYPE_PHONE, 'Phone'),
)

DEFAULT_TIMEOUT = 5

REGION_NEW_YORK = 1
REGION_AMSTERDAM = 2
REGION_SINGAPORE = 3

DEFAULT_REGION = REGION_AMSTERDAM

REGION_CHOICES = (
    (REGION_NEW_YORK, 'New York'),
    (REGION_AMSTERDAM, 'Amsterdam'),
    (REGION_SINGAPORE, 'Singapore'),
)

REGION_CHOICES_DICT = dict(REGION_CHOICES)

REGION_CHOICES_ABBREVIATED = (
    (REGION_NEW_YORK, 'NY'),
    (REGION_AMSTERDAM, 'AM'),
    (REGION_SINGAPORE, 'SG'),
)

REGION_DOMAINS = (
    (REGION_NEW_YORK, 'newyork.watchyouwork.com'),
    (REGION_AMSTERDAM, 'amsterdam.watchyouwork.com'),
    (REGION_SINGAPORE, 'singapore.watchyouwork.com'),
)

REGION_COORDINATES = (
    (REGION_NEW_YORK, (40.749, -73.9865)),
    (REGION_AMSTERDAM, (52.374, 4.8897)),
    (REGION_SINGAPORE, (1.2931, 103.8558)),
)


class RequestException(Exception):
    pass


class RequestException400BadRequest(RequestException):
    pass


class RequestException401Unauthorized(RequestException):
    pass


class RequestException403Forbidden(RequestException):
    pass


class RequestException404NotFound(RequestException):
    pass


class RequestException405NotAllowed(RequestException):
    pass


class RequestExceptionFormNotValid(RequestException):
    pass
