# -*- coding: utf8 -*-
from django.contrib.messages import add_message, SUCCESS
from django.shortcuts import redirect, render

from main.forms.admin.upload_monitors import UploadMonitorsForm


def upload_monitors(request):
    form = UploadMonitorsForm(request.POST or None, files=request.FILES or None)

    if form.is_valid():
        form.save()
        add_message(request, SUCCESS, 'Monitors imported')

        return redirect('/admin/')
    return render(request, 'admin/upload_monitors.html', {'form': form, 'title': 'Upload monitors'})
