# -*- coding:utf-8 -*-
from json import dumps

from django.contrib.auth import login
from django.http import HttpResponse

from main.forms.auth.login import LoginForm


def api_login(request):
    form = LoginForm(request.GET)
    if form.is_valid():
        user = form.save()
        login(request, user)
        response = {'status': 'ok'}
    else:
        response = {'status': 'error', 'errors': form.errors}

    return HttpResponse(dumps(response), content_type='application/json')
