# -*- coding:utf-8 -*-
from json import dumps

from django.contrib import messages
from django.http import HttpResponse

from main.forms.auth.reset_password import ResetPasswordForm


def api_reset_password(request):
    form = ResetPasswordForm(request.GET)
    if form.is_valid():
        form.save()
        messages.success(request, 'We sent password reset link to %s' % form.cleaned_data['email'])
        response = {'status': 'ok'}
    else:
        response = {'status': 'error', 'errors': form.errors}

    return HttpResponse(dumps(response), content_type='application/json')
