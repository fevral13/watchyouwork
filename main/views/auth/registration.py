# -*- coding:utf-8 -*-
from json import dumps

from django.contrib.auth import login
from django.http import HttpResponse

from main.forms.auth.registration import RegistrationForm


def api_registration(request):
    form = RegistrationForm(request.GET)
    if form.is_valid():
        user = form.save(request=request)
        login(request, user)
        response = {'status': 'ok'}
    else:
        response = {'status': 'error', 'errors': form.errors}

    return HttpResponse(dumps(response), content_type='application/json')
