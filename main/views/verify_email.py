# -*- coding:utf-8 -*-
from django.shortcuts import render

from main.models import Contact
from main.models.contact import serializer


def verify_email(request, verification_token):
    data = serializer.loads(verification_token)
    try:
        c = Contact.objects.get(id=data['id'], value=data['value'])
    except (Contact.DoesNotExist, KeyError):
        message = 'Wrong token'
    else:
        c.mark_verified()
        message = 'Email %s verified!' % c.value

    return render(request, 'verify_email.html', {'message': message})
