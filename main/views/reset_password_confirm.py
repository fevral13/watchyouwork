# -*- coding:utf-8 -*-
from django.contrib import messages
from django.shortcuts import render, redirect

from main.forms.auth.reset_password_confirm import ResetPasswordConfirmForm


def reset_password_confirm(request, reset_token):
    form = ResetPasswordConfirmForm(initial={'token': reset_token}, data=request.POST or None)

    if form.is_valid():
        form.save()
        messages.success(request, 'Password changed')
        return redirect('/#login')

    return render(request, 'reset_password_confirm.html', {'form': form})
