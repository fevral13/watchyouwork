# -*- coding: utf8 -*-
import logging
import socket

from geoip import geolite2
from geopy import distance, Point
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from main import DEFAULT_REGION, REGION_COORDINATES, REGION_CHOICES_DICT
from main.utils import get_domain_from_url

logger = logging.getLogger(__name__)


def _get_distance(l1, l2):
    """
    Calculate distance between two coordinates - (lat, lon) pairs
    :param l1: (lat, lon)
    :param l2: (lat, lon)
    :return: distance
    """
    return distance.distance(Point(l1), Point(l2)).km


class RegionAPI(APIView):
    """
    Used to auto-detect nearest monitor region for url. Will return default
    region in case of failure
    """
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request):
        url = request.query_params.get('url', '')
        # default data will be returned if autodetection fails
        data = {'region': DEFAULT_REGION,
                'region_name': REGION_CHOICES_DICT[DEFAULT_REGION],
                'guess_ok': False}

        domain = get_domain_from_url(url)

        try:
            # resolve ip address
            ip = socket.gethostbyname(domain)
            match = geolite2.lookup(ip)

        # if socket.gethostbyname(domain) can't resolve IP
        except socket.gaierror:
            logger.warning('Error resolving IP for domain %s', domain)

        # if geolite2.lookup(ip) fails
        except ValueError:
            logger.warning('Error getting GEO info for ip %s', ip)
        else:
            if match:
                # if ip address was found in geo ip db, get its coordinates
                location = match.location
                ip_coordinates = Point(location)

                # calculate distances between monitor datacenters and found
                # location
                distances = sorted((_get_distance(ip_coordinates,
                                                  region_coordinates),
                                    region_id)
                                   for region_id, region_coordinates
                                   in REGION_COORDINATES)

                closest_point = distances[0]
                closest_region_id = closest_point[1]

                data = {'region': closest_region_id,
                        'region_name': REGION_CHOICES_DICT[closest_region_id],
                        'guess_ok': True}
        return Response(data)
