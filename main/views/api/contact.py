# -*- coding: utf8 -*-
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from main.models import Contact
from main.models.contact import ContactSerializer


class BaseContactAPI(object):
    """
    Comon properties and methods for Contact API views
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = ContactSerializer

    def get_queryset(self):
        return Contact.objects.filter(user=self.request.user)


class ContactListAPI(BaseContactAPI, generics.ListCreateAPIView):
    pass


class ContactDetailAPI(BaseContactAPI, generics.RetrieveUpdateDestroyAPIView):
    def get_object(self):
        return self.get_queryset().get(id=self.kwargs['pk'])
