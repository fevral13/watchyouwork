# -*- coding: utf8 -*-

from rest_framework import permissions
from rest_framework.generics import ListAPIView

from main.models.monitor_event import MonitorEventSerializer, MonitorEvent
from main.utils import LegacyPaginationClass


class MonitorEventsAPI(ListAPIView):
    serializer_class = MonitorEventSerializer
    permission_classes = (permissions.IsAuthenticated, )
    pagination_class = LegacyPaginationClass

    def get_queryset(self):
        qs = MonitorEvent.objects \
            .select_related('previous',
                            'monitor',
                            'monitor__interval_schedule') \
            .filter(monitor__user=self.request.user) \
            .order_by('-id')
        monitor_id = self.request.query_params.get('monitor_id')
        if monitor_id:
            qs = qs.filter(monitor=monitor_id)
        return qs
