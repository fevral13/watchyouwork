# -*- coding: utf8 -*-
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Monitor


class MonitorStatusAPI(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        statuses = {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
        }
        statuses.update(Monitor.objects.monitor_statuses_dict(request.user))
        return Response(statuses)
