# -*- coding: utf8 -*-
from rest_framework.generics import RetrieveUpdateAPIView
from rest_framework.permissions import IsAuthenticated

from main.models.profile import ProfileSerializer


class ProfileAPI(RetrieveUpdateAPIView):
    permission_classes = (IsAuthenticated, )
    serializer_class = ProfileSerializer

    def get_object(self):
        return self.request.user.profile

    def post(self, *args, **kwargs):
        """
        Normally POST is used to create object, but to keep backwards
        compatibility, POST updates it
        :param args:
        :param kwargs:
        :return:
        """
        return self.put(*args, **kwargs)
