# -*- coding: utf8 -*-
from rest_framework import generics, permissions

from main.models import Monitor
from main.models.monitor import MonitorSerializer
from main.utils import LegacyPaginationClass


class BaseMonitorAPI(object):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = MonitorSerializer

    def _get_qs(self):
        qs = Monitor.objects.select_related('interval_schedule') \
            .filter(user=self.request.user)

        return qs


class MonitorCreateListAPI(BaseMonitorAPI, generics.ListCreateAPIView):
    pagination_class = LegacyPaginationClass

    def get_queryset(self):
        qs = self._get_qs()

        status = self.request.GET.get('status', None)
        if status is not None:
            qs = qs.filter(status=status)

        return qs


class MonitorUpdateAPI(BaseMonitorAPI, generics.RetrieveUpdateDestroyAPIView):
    def get_queryset(self):
        return self._get_qs()

    def put(self, request, *args, **kwargs):
        was_running = self.get_object().running
        response = super(MonitorUpdateAPI, self).put(request, *args, **kwargs)

        monitor = self.get_object()
        # if monitor 'running' changes, pause/resume it accordingly
        if was_running and not monitor.running:
            monitor.pause()
        elif not was_running and monitor.running:
            monitor.resume()

        return response

