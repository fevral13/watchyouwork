# -*- coding: utf8 -*-
from django.contrib.auth import authenticate, login
from rest_framework import permissions, serializers
from rest_framework.generics import UpdateAPIView


class PasswordChangeSerializer(serializers.Serializer):
    """
    Validates presence of "password" field.

    Updates current user's password.
    """
    password = serializers.CharField(write_only=True)

    def update(self, user, validated_data):
        password = validated_data['password']
        user.set_password(password)
        user.save()

        # relogin user again, because he gets kicked out
        # after password change
        u = authenticate(username=user.email, password=password)
        login(self.context['request'], u)
        return user


class ChangePasswordAPI(UpdateAPIView):
    """
    Used to update current user's password
    """
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = PasswordChangeSerializer

    def get_object(self):
        return self.request.user
