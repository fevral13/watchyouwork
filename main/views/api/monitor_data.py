# -*- coding: utf8 -*-
from rest_framework import generics, permissions

from main.models import MonitorData
from main.models.monitor_data import MonitorDataSerializer


class MonitorDataAPI(generics.ListAPIView):
    serializer_class = MonitorDataSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        interval = int(self.request.query_params.get('interval') or 5)
        duration = int(self.request.query_params.get('duration') or 24)
        monitor_id = self.kwargs['monitor_id']
        return MonitorData.objects.data_for_monitor(monitor_id=monitor_id,
                                                    interval=interval,
                                                    duration=duration)
