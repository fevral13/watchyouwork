# -*- coding: utf8 -*-
from rest_framework import permissions, serializers
from rest_framework.generics import UpdateAPIView

from main import CONTACT_TYPE_PHONE
from main.models import Contact


class InitVerifyPhoneSerializer(serializers.Serializer):
    """
    Validates that contact_id belongs to current user.

    Resends verification SMS upon update()
    """
    contact_id = serializers.IntegerField(write_only=True)

    def validate_contact_id(self, contact_id):
        try:
            return Contact.objects.get(id=contact_id,
                                       contact_type=CONTACT_TYPE_PHONE,
                                       user=self.context['request'].user)
        except Contact.DoesNotExist:
            raise serializers.ValidationError('Object not found')

    def update(self, instance, validated_data):
        validated_data['contact_id'].send_verification()
        return self.user


class VerifyPhoneSerializer(InitVerifyPhoneSerializer):
    """
    Inherits contact_id from its parent, plus validates that SMS code is correct.

    Marks contact as verified upon update()
    """
    code = serializers.CharField(write_only=True)

    def validate(self, validated_data):
        if not validated_data['contact_id']\
                .verify_sms_code(validated_data['code']):
            raise serializers.ValidationError({'code': 'Wrong code!'})

    def update(self, instance, validated_data):
        validated_data['contact_id'].mark_verified()
        return instance


class InitVerifyPhoneAPI(UpdateAPIView):
    """
    (Re)sends verification SMS code
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = InitVerifyPhoneSerializer

    def get_object(self):
        return self.request.user


class VerifyPhoneAPI(UpdateAPIView):
    """
    Enters verification code from SMS
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = VerifyPhoneSerializer

    def get_object(self):
        return self.request.user
