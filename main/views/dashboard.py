# -*- coding:utf-8 -*-
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.cache import cache
from django.shortcuts import render
from djcelery.models import IntervalSchedule

from main import MONITOR_DATA_STATUS_CHOICES, REGION_CHOICES, \
    CONTACT_TYPE_CHOICES, MONITOR_TYPE_CHOICES, REGION_CHOICES_DICT
from main.views.base_classes import dumps
from websockets.helpers import create_websockets_auth_token


@login_required
def dashboard(request):
    template = 'dashboard.html'
    cache_key = 'interval_schedule_choices'
    interval_schedule_choices = cache.get(cache_key)
    if interval_schedule_choices is None:
        interval_schedule_choices = {i.id: str(i) for i in IntervalSchedule.objects.all()}
        cache.set(cache_key, interval_schedule_choices, 60 * 5)

    context = {
        'initial': dumps({
            'statuses': dict(MONITOR_DATA_STATUS_CHOICES),
            'regions': REGION_CHOICES_DICT,
            'contactTypes': dict(CONTACT_TYPE_CHOICES),
            'monitorTypes': dict(MONITOR_TYPE_CHOICES),
            'intervalSchedules': interval_schedule_choices,
            'ws_path': settings.WS_PATH,
            'ws_auth_token': create_websockets_auth_token(request.user.id)
        })
    }

    return render(request, template, context)
