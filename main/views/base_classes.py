# -*- coding: utf8 -*-
from datetime import date, timedelta
from decimal import Decimal
from functools import partial
from json import JSONEncoder, dumps as standard_dumps

from django.db.models import Model, QuerySet


def td_format(td_object):
    seconds = int(td_object.total_seconds())
    periods = [
        (u'y.', 60 * 60 * 24 * 365),
        (u'mon.', 60 * 60 * 24 * 30),
        (u'd.', 60 * 60 * 24),
        (u'h.', 60 * 60),
        (u'min.', 60),
        (u'sec.', 1)
    ]

    strings = []
    for period_name, period_seconds in periods:
        if seconds > period_seconds:
            period_value, seconds = divmod(seconds, period_seconds)
            if period_value == 1:
                strings.append(u"%s %s" % (period_value, period_name))
            else:
                strings.append(u"%s %s" % (period_value, period_name))

    return u", ".join(strings)


class CustomJSONEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Decimal):
            return float(o)

        if isinstance(o, date):
            return o.strftime('%d/%m/%Y %H:%M:%S')

        if isinstance(o, QuerySet):
            return list(o)

        if isinstance(o, timedelta):
            return td_format(o)

        if isinstance(o, Model):
            return o.to_dict()

        return super(CustomJSONEncoder, self).default(o)


dumps = partial(standard_dumps, cls=CustomJSONEncoder)
