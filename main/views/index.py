# -*- coding:utf-8 -*-
from django.shortcuts import render, redirect


def index(request):
    if request.user.is_authenticated():
        return redirect('/dashboard/')
    else:
        template = 'index.html'

    return render(request, template)
