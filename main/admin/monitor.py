# -*- coding:utf-8 -*-
from django.contrib import admin

from main.models import Monitor, MonitorEvent


class MonitorEventInline(admin.TabularInline):
    model = MonitorEvent
    extra = 0


class MonitorAdmin(admin.ModelAdmin):
    inlines = (MonitorEventInline,)
    list_display = ('user', 'url', 'name', 'interval_schedule', 'running', 'type')
    list_filter = ('type', 'status', 'running')


admin.site.register(Monitor, MonitorAdmin)
