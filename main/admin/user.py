# -*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from main.models import Contact


class ContactInline(admin.TabularInline):
    model = Contact
    extra = 0


class NewUserAdmin(UserAdmin):
    inlines = (ContactInline,)


admin.site.unregister(User)
admin.site.register(User, NewUserAdmin)
