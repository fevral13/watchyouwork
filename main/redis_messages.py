# -*- coding: utf8 -*-
import logging

from django.conf import settings
from redis import StrictRedis

from main.views.base_classes import dumps

client = StrictRedis(host=settings.REDIS_HOST, db=settings.REDIS_DB)
logger = logging.getLogger(__name__)


def publish_message(user_id, channel, message):
    data = {'data': message, 'channel': channel}

    from pprint import pformat
    logger.info(pformat(data))

    client.publish('user_%s' % user_id, dumps(data))
