# -*- coding:utf-8 -*-
import six
from rest_framework import serializers
from django.db import models
from django.utils.timezone import now, localtime

from main import MONITOR_DATA_STATUS_CHOICES
from main.models.monitor import MonitorSerializer
from main.redis_messages import publish_message


@six.python_2_unicode_compatible
class MonitorEvent(models.Model):
    monitor = models.ForeignKey('main.Monitor', related_name='events')
    type = models.IntegerField(choices=MONITOR_DATA_STATUS_CHOICES)
    duration = models.DurationField(blank=True, null=True)
    datetime_created = models.DateTimeField()
    previous = models.ForeignKey('self', blank=True, null=True)

    class Meta:
        app_label = 'main'

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        is_new = self.id is None
        if is_new:  # if created
            self.datetime_created = self.datetime_created or now()
            last_monitor_event = MonitorEvent.objects.only('datetime_created'). \
                filter(monitor=self.monitor). \
                order_by('-datetime_created'). \
                first()
            if last_monitor_event:
                last_monitor_event.duration = self.datetime_created - last_monitor_event.datetime_created
                last_monitor_event.save()
                self.previous = last_monitor_event

        super(MonitorEvent, self).save(*args, **kwargs)
        if is_new:
            publish_message(self.monitor.user_id,
                            'event:new',
                            MonitorEventSerializer(self).data)


class ShortMonitorSerializer(serializers.ModelSerializer):
    type = serializers.CharField(source='get_type_display', read_only=True)
    type_id = serializers.IntegerField(source='type', read_only=True)
    monitor_id = serializers.IntegerField(source='monitor.id')

    class Meta:
        model = MonitorEvent
        fields = ('id', 'type', 'type_id', 'duration', 'monitor_id',
                  'monitor',
                  'datetime_created')


class MonitorEventSerializer(ShortMonitorSerializer):
    previous = ShortMonitorSerializer(read_only=True)
    monitor = MonitorSerializer(read_only=True)

    class Meta:
        model = MonitorEvent
        fields = ShortMonitorSerializer.Meta.fields + ('previous', )
