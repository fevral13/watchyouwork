# -*- coding:utf-8 -*-
import random
import logging

import six
from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.utils.timezone import get_current_timezone_name, activate, localtime
from itsdangerous import TimedJSONWebSignatureSerializer
from rest_framework import serializers, fields

from main import CONTACT_TYPE_CHOICES, CONTACT_TYPE_EMAIL, CONTACT_TYPE_PHONE
from main.redis_messages import publish_message

serializer = TimedJSONWebSignatureSerializer(settings.SECRET_KEY,
                                             expires_in=60 * 60 * 24)
logger = logging.getLogger(__name__)


@six.python_2_unicode_compatible
class Contact(models.Model):
    user = models.ForeignKey('auth.User', related_name='contacts')
    contact_type = models.IntegerField(choices=CONTACT_TYPE_CHOICES)
    value = models.CharField(max_length=255)
    verified = models.BooleanField(default=False, blank=True)
    active = models.BooleanField(default=True, blank=True)

    class Meta:
        app_label = 'main'

    def __str__(self):
        return str(self.user)

    def __init__(self, *args, **kwargs):
        super(Contact, self).__init__(*args, **kwargs)
        self.__attr_cache = self.__get_attr_cache()

    def __get_attr_cache(self):
        return {
            'contact_type': self.contact_type,
            'value': self.value
        }

    def save(self, *args, **kwargs):
        if self.__attr_cache != self.__get_attr_cache():
            self.verified = False
            self.send_verification()

        super(Contact, self).save(*args, **kwargs)

    def send_verification(self):
        if self.contact_type == CONTACT_TYPE_EMAIL:
            self.send_verification_email()
        elif self.contact_type == CONTACT_TYPE_PHONE:
            self.send_verification_sms()

    def send_verification_email(self):
        from main.tasks import send_email
        send_email.delay(
            to=self.value,
            subject=u'Verify email address',
            text_template='emails/verification.txt',
            html_template='emails/verification.html',
            context={
                'verification_token': self.generate_email_verification_token()
            }
        )
        publish_message(self.user_id,
                        'common:alert',
                        {'type': 'alert-success',
                         'value': u'Verification link sent to %s' % self.value})

    def send_verification_sms(self):
        from main.tasks import send_sms
        code = ''.join(str(random.choice(range(10))) for _ in xrange(6))

        cache.set(self.cache_key(), code, 60 * 60)
        send_sms.delay(
            to=self.value,
            text_template='sms/verification.txt',
            context={
                'code': code
            }
        )
        logger.debug('Verification SMS code %s sent to %s', code, self.value)
        publish_message(self.user_id,
                        'common:alert',
                        {'type': 'alert-success',
                         'value': u'Verification code sent to %s' % self.value})

    def notify_state_change(self, monitor, monitor_data):
        current_timezone = get_current_timezone_name()
        activate(monitor.user.profile.timezone)
        context = {
            'monitor': monitor.short_description()[:80],
            'region': monitor.region_abbreviated(),
            'region_full': monitor.get_region_display(),
            'time': localtime(monitor_data.datetime_created).strftime('%H:%M:%S'),
            'status': monitor_data.get_status_display()
        }

        if self.contact_type == CONTACT_TYPE_EMAIL:
            from main.tasks import send_email
            send_email.delay(
                to=self.value,
                subject=u'Monitor %s %s' % (monitor.short_description()[:80], monitor_data.get_status_display()),
                text_template='emails/monitor_status_change.txt',
                html_template='emails/monitor_status_change.html',
                context=context
            )

        elif self.contact_type == CONTACT_TYPE_PHONE:
            from main.tasks import send_sms
            send_sms.delay(
                to=self.value,
                text_template='sms/monitor_status_change.txt',
                context=context
            )
        activate(current_timezone)

    def verify_sms_code(self, code):
        return cache.get(self.cache_key()) == code

    def cache_key(self):
        return self.value

    def generate_email_verification_token(self):
        return serializer.dumps({'value': self.value, 'id': self.id})

    def mark_verified(self):
        self.verified = True
        self.save(update_fields=['verified'])
        user = self.user
        if self.contact_type == CONTACT_TYPE_EMAIL and user.email == self.value:
            profile = user.profile
            profile.send_welcome_email()

        publish_message(self.user_id,
                        'contact:verified',
                        ContactSerializer(self).data)


class ContactSerializer(serializers.ModelSerializer):
    user = fields.HiddenField(default=serializers.CurrentUserDefault())
    contact_type_name = fields.CharField(source='get_contact_type_display',
                                         read_only=True)
    verified = fields.BooleanField(read_only=True)

    class Meta:
        model = Contact
        fields = ('id', 'contact_type', 'contact_type_name', 'value',
                  'verified', 'active', 'user')
