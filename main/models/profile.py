# -*- coding:utf-8 -*-
import six
from django.conf import settings
from django.db import models
from django.db.models import signals
from geoip import geolite2
from rest_framework import serializers, fields

from main import TIMEZONE_CHOICES


@six.python_2_unicode_compatible
class Profile(models.Model):
    """
    User's profile
    """
    user = models.OneToOneField('auth.User', related_name='profile')
    timezone = models.CharField(max_length=40,
                                choices=TIMEZONE_CHOICES,
                                default=settings.TIME_ZONE)
    welcome_email_sent = models.BooleanField(blank=True, default=False)

    class Meta:
        app_label = 'main'

    def __str__(self):
        return str(self.user)

    def send_welcome_email(self):
        """
        Sends Welcome email if it has not been sent already. Sets
        welcome_email_sent to True
        :return:
        """
        if not self.welcome_email_sent:
            from main.tasks import send_email
            send_email.delay(
                to=self.user.email,
                subject='Welcome to WatchYouWork.com',
                text_template='emails/welcome.txt',
                html_template='emails/welcome.html',
                context={
                    'profile': self
                }
            )
            self.welcome_email_sent = True
            self.save(update_fields=['welcome_email_sent'])

    def guess_timezone(self, ip):
        """
        Set current user's timezone by given IP address.

        Performs lookup in geoip database. Does nothing if lookup did not yield
        timezone info.
        :param ip: user's IP address
        :return:
        """
        try:
            match = geolite2.lookup(ip)
        except ValueError:
            pass
        else:
            if match:
                tz = match.timezone
                if tz in dict(TIMEZONE_CHOICES):
                    self.timezone = tz
                    self.save(update_fields=['timezone'])

    def to_dict(self):
        return {
            'timezone': self.timezone,
            'email': self.user.email
        }


class ProfileSerializer(serializers.ModelSerializer):
    user = fields.HiddenField(default=serializers.CurrentUserDefault())
    email = fields.EmailField(source='user.email', read_only=True)

    class Meta:
        model = Profile
        fields = ('timezone', 'email', 'user')


def create_profile_for_new_user(instance, created, **kwargs):
    """
    Signal handler to create user Profile for new users.

    Useful when user instance is created from CLI with createsuperuser.
    """
    if created:
        Profile.objects.create(user=instance)


signals.post_save.connect(create_profile_for_new_user,
                          sender='auth.User',
                          dispatch_uid='create_profile_for_new_user')
