# -*- coding:utf-8 -*-
from collections import namedtuple

import six
from django.db import models, connection
from django.utils.timezone import now, localtime
from rest_framework import serializers

from main import MONITOR_DATA_STATUS_CHOICES, REGION_CHOICES


def _convert_data(args):
    date, status, duration = args
    return localtime(date).strftime('%H:%M'), status, duration.microseconds / 1000


monitor_data_type = namedtuple('monitor_data_type', ('date', 'status', 'duration'))


class MonitorDataManager(models.Manager):
    def data_for_monitor(self, monitor_id, interval, duration=24):
        with connection.cursor() as cursor:
            cursor.execute('''select * from monitor_data_status_grouped(%s, interval '%s minutes', interval '%s hours')''',
                           [monitor_id, interval, duration])
            data = cursor.fetchall()
        return [monitor_data_type(*d) for d in data]

    def create_new_record(self, monitor_id, datetime_created, status, duration,
                          region, response_status_code):
        with connection.cursor() as cursor:
            query = '''
            insert into main_monitordata_{id} (monitor_id, datetime_created, status, duration, region, response_status_code)
            values (%s, %s, %s, %s, %s, %s) returning id;
            '''.format(id=monitor_id)

            cursor.execute(query, [monitor_id, datetime_created, status,
                                   duration, region, response_status_code])
            data_id = cursor.fetchone()[0]
        return data_id


@six.python_2_unicode_compatible
class MonitorData(models.Model):
    monitor_id = models.IntegerField()
    status = models.IntegerField(choices=MONITOR_DATA_STATUS_CHOICES)
    response_status_code = models.IntegerField(default=0)
    duration = models.DurationField()
    region = models.IntegerField(choices=REGION_CHOICES)
    datetime_created = models.DateTimeField()

    objects = MonitorDataManager()

    class Meta:
        app_label = 'main'

    def save(self, *args, **kwargs):
        if self.id is None:
            self.datetime_created = self.datetime_created or now()

        super(MonitorData, self).save(*args, **kwargs)

    def __str__(self):
        return str(self.id)


class MilisecondsDurationField(serializers.DurationField):
    def to_representation(self, value):
        return value.microseconds / 1000


class MonitorDataSerializer(serializers.Serializer):
    date = serializers.DateTimeField()
    status = serializers.IntegerField()
    duration = MilisecondsDurationField()
