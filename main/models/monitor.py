# -*- coding:utf-8 -*-
import json

import celery
from datetime import timedelta

import six
from django.db import models, connection
from django.db.models.signals import post_save
from django.utils.timezone import now
from rest_framework import serializers

from rest_framework.exceptions import ValidationError

from main import MONITOR_TYPE_CHOICES, MONITOR_DATA_STATUS_CHOICES, \
    MONITOR_TASKS, MONITOR_DATA_STATUS_UNKNOWN, \
    MONITOR_DATA_STATUS_PAUSED, MONITOR_DATA_STATUS_STARTED, REGION_CHOICES, \
    REGION_CHOICES_ABBREVIATED, MONITOR_TYPE_HTTP
from main.redis_messages import client


class MonitorManager(models.Manager):
    def monitor_statuses(self, user):
        return self.filter(user=user).values('status').annotate(count=models.Count('*'))

    def monitor_statuses_dict(self, user):
        return {s['status']: s['count'] for s in self.monitor_statuses(user)}


@six.python_2_unicode_compatible
class Monitor(models.Model):
    user = models.ForeignKey('auth.User')
    type = models.IntegerField(choices=MONITOR_TYPE_CHOICES)
    running = models.BooleanField(blank=True, db_index=True, default=True)
    region = models.IntegerField(choices=REGION_CHOICES)
    interval_schedule = models.ForeignKey('djcelery.IntervalSchedule')
    periodic_task = models.ForeignKey('djcelery.PeriodicTask', blank=True, null=True, on_delete=models.SET_NULL)
    status = models.IntegerField(choices=MONITOR_DATA_STATUS_CHOICES, default=MONITOR_DATA_STATUS_UNKNOWN)
    name = models.CharField(max_length=255, blank=True)
    url = models.TextField()
    args = models.TextField(default='{}')  # todo: replace with JSONField
    datetime_created = models.DateTimeField()
    datetime_modified = models.DateTimeField()

    objects = MonitorManager()

    class Meta:
        app_label = 'main'

    def __str__(self):
        return self.url

    @property
    def args_data(self):
        try:
            return self._args_data
        except AttributeError:
            self._args_data = json.loads(self.args)
        return self._args_data

    @args_data.setter
    def args_data(self, val):
        self._args_data = val
        self.args = json.dumps(val)

    def save(self, *args, **kwargs):
        is_new = self.id is None
        self.datetime_created = self.datetime_created or now()
        self.datetime_modified = now()

        from djcelery.models import PeriodicTask

        task_type = dict(MONITOR_TASKS)[self.type]
        task_kwargs = {'url': self.url, 'monitor_id': self.id, 'kwargs': self.args_data}
        queue_name = 'region_%s' % self.region

        if self.running:
            pt = self.periodic_task and self.periodic_task or PeriodicTask()
            pt.name = u'%s: %s' % (self.id, self.url[:100])
            pt.task = task_type
            pt.interval = self.interval_schedule
            pt.queue = queue_name
            pt.kwargs = json.dumps(task_kwargs)
            pt.save()
            self.periodic_task = pt

        super(Monitor, self).save(*args, **kwargs)

        if is_new:
            with connection.cursor() as cursor:
                query = '''create table main_monitordata_{id} (check (monitor_id=%s)) inherits (main_monitordata);'''.format(id=self.id)
                cursor.execute(query, [self.id])

            if self.running:
                self.run_job_immediately()

    def run_job_immediately(self):
        if self.running:
            pt = self.periodic_task

            celery.execute.send_task(
                pt.task,
                kwargs=json.loads(pt.kwargs),
                queue=pt.queue,
                eta=now() + timedelta(seconds=1)
            )

    def pause(self):
        self.running = False
        self.status = MONITOR_DATA_STATUS_PAUSED
        client.delete(self.status_cache_key())

        if self.periodic_task:
            self.periodic_task.delete()
            self.periodic_task = None

        self.save()

        from main.models import MonitorEvent

        MonitorEvent.objects.create(
            monitor=self,
            type=MONITOR_DATA_STATUS_PAUSED
        )

    def resume(self):
        self.running = True
        self.status = MONITOR_DATA_STATUS_STARTED
        self.save()
        from main.models import MonitorEvent

        MonitorEvent.objects.create(
            monitor=self,
            type=MONITOR_DATA_STATUS_STARTED
        )
        self.run_job_immediately()

    def delete(self, *args, **kwargs):
        with connection.cursor() as cursor:
            query = '''drop table if exists main_monitordata_{id};'''.format(id=self.id)
            cursor.execute(query)

        if self.periodic_task:
            self.periodic_task.delete()
        super(Monitor, self).delete(*args, **kwargs)

    def last_data_record(self):
        return self.monitordata_set.order_by('-datetime_created')[0]

    def on_status_change(self, monitor_data):
        # send messages everywhere
        contacts = self.user.contacts.filter(active=True, verified=True)
        for contact in contacts:
            contact.notify_state_change(self, monitor_data)

    def short_description(self):
        return u'{type}: {url}'.format(type=self.get_type_display(), url=self.url)

    def region_abbreviated(self):
        return dict(REGION_CHOICES_ABBREVIATED)[self.region]

    def status_cache_key(self):
        return 'monitor_status_%s' % self.id

    def to_dict(self, full=False):
        data = {
            'id': self.id,
            'type': self.type,
            'type_name': self.get_type_display(),
            'running': self.running,
            'region': self.region,
            'region_name': self.get_region_display(),
            'interval_schedule': self.interval_schedule_id,
            'interval_schedule_name': str(self.interval_schedule),
            'status': self.status,
            'status_name': self.get_status_display(),
            'name': self.name,
            'url': self.url,
        }
        data.update(self.args_data)
        if full:
            from main.models.monitor_event import ShortMonitorSerializer
            data['events'] = ShortMonitorSerializer(self.events.all(), many=True).data
        return data


def create_monitor_event(created, instance, *args, **kwargs):
    if created:
        instance.resume()


post_save.connect(create_monitor_event,
                  sender=Monitor,
                  dispatch_uid='monitor_created')


class MonitorSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    type_name = serializers.CharField(source='get_type_display', read_only=True)
    status_name = serializers.CharField(source='get_status_display',
                                        read_only=True)
    region_name = serializers.CharField(source='get_region_display',
                                        read_only=True)
    interval_schedule_name = serializers.CharField(
        source='interval_schedule',
        read_only=True)
    args = serializers.JSONField(source='args_data')

    def validate(self, attrs):
        _attrs = super(MonitorSerializer, self).validate(attrs)

        # additional validation of args for monitor type HTTP
        if _attrs['type'] == MONITOR_TYPE_HTTP:

            args = _attrs['args_data']
            should = args.get('should')
            # convert 'should' to int, if it is present
            if should:
                args['should'] = int(should)

            # keyword must be passed if it 'should' be present
            if args.get('keyword') and args.get('should') is None:
                raise ValidationError({'should',
                                       'Required if keyword is entered'})
            # password without username does not make sense
            if args.get('password') and not args.get('username'):
                raise ValidationError({'username',
                                       'Required if password is entered'})

        return _attrs

    class Meta:
        model = Monitor
        fields = ('id', 'user',
                  'type', 'type_name',
                  'running', 'name', 'url',
                  'region', 'region_name',
                  'status', 'status_name',
                  'interval_schedule', 'interval_schedule_name',
                  'args',
                  )
