# -*- coding: utf8 -*-
from django.contrib.auth.models import User

from django.core.management import BaseCommand


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('email', type=str)

    def handle(self, email, *args, **options):
        User.objects.filter(email=email).update(is_staff=True, is_superuser=True)
