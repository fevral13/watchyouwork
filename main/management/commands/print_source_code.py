# -*- coding: utf8 -*-
import os

from django.conf import settings
from django.core.management import BaseCommand

include_extensions = ['.js', '.py', '.css', '.txt']
exclude_directories = [
    'static/vendor',
    'static/js/dashboard_app_build',
    'static/js/index_app_build',
    'static/vendor/jqueryui',
    'static/vendor/bootstrap',
    'static/vendor/bootstrap/css',
]

exclude_files = [
    'settings_local.py',
    'fabfile_config_local.py',
    'manage.py',
    'wsgi.py',
    'requirements.txt',
    'fabfile.py',
    'print_source_code.py',
]


def searcher(filename):
    for ext in include_extensions:
        if filename.endswith(ext):
            return True


class Command(BaseCommand):
    def handle(self, *args, **options):
        walker = os.walk(settings.BASE_DIR)

        for directory, _, files in walker:
            if os.path.relpath(directory, settings.BASE_DIR) in exclude_directories:
                continue

            for file in filter(searcher, files):
                if file in exclude_files:
                    continue

                filepath = (os.path.relpath(os.path.join(settings.BASE_DIR, directory, file)), settings.BASE_DIR)[0]

                with open(os.path.join(settings.BASE_DIR, directory, file), 'r') as f:
                    contents = f.read()

                if len(contents) < 5 \
                        or contents.strip() == '# -*- coding: utf-8 -*-' \
                        or contents.strip() == '# -*- coding:utf-8 -*-' \
                        or contents.strip() == '# -*- coding: utf8 -*-':
                    continue
                print('== %s ==' % filepath)
                print contents
                print(u'== end %s ==\n\n' % filepath).encode('utf8')
