# -*- coding: utf8 -*-
from datetime import timedelta
from django.core.management import BaseCommand
from django.utils.timezone import now

from main.models import MonitorData, MonitorEvent


class Command(BaseCommand):
    def handle(self, *args, **options):
        MonitorData.objects.filter(datetime_created__lte=now() - timedelta(days=1)).delete()
        MonitorEvent.objects.filter(datetime_created__lte=now() - timedelta(days=5)).delete()
