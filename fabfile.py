# -*- coding:utf-8 -*-
from fabric.api import env, sudo, cd
from fabric.decorators import task

try:
    from fabfile_config_local import PASSWORD
except ImportError:
    pass
else:
    env.password = PASSWORD

NEW_YORK = 'newyork.watchyouwork.com'
AMSTERDAM = 'amsterdam.watchyouwork.com'
SINGAPORE = 'singapore.watchyouwork.com'

env.roledefs = {
    'ny': [NEW_YORK],
    'am': [AMSTERDAM],
    'sg': [SINGAPORE],
}

CONFIG = {
    NEW_YORK: {
        'sources_folder': '/home/django/projects/watchyouwork',
        'interpreter': '/home/django/.virtualenvs/watchyouwork/bin/python',
        'supervisorctl': 'supervisorctl',
        'celery_tasks': 'watchyouwork-celery watchyouwork-celery-savedata watchyouwork-ws',
        'web': True,
    },
    AMSTERDAM: {
        'sources_folder': '/home/django/projects/watchyouwork',
        'interpreter': '/home/django/.virtualenvs/watchyouwork/bin/python',
        'supervisorctl': 'supervisorctl',
        'celery_tasks': 'watchyouwork-celery',
    },
    SINGAPORE: {
        'sources_folder': '/home/django/projects/watchyouwork',
        'interpreter': '/home/django/.virtualenvs/watchyouwork/bin/python',
        'supervisorctl': 'supervisorctl',
        'celery_tasks': 'watchyouwork-celery',
    },

}

@task
def deploy():
    env.settings = CONFIG[env['host']]

    with cd(env.settings['sources_folder']):
        sudo('git pull', user='django')
        if 'web' in env.settings:
            sudo('%(interpreter)s manage.py migrate --noinput' % env.settings, user='django')
            sudo('%(interpreter)s manage.py collectstatic --noinput' % env.settings, user='django')
            sudo('kill -HUP $(cat pidfile)', user='django')

        sudo('%(supervisorctl)s restart %(celery_tasks)s' % env.settings)

@task
def install():
    env.settings = CONFIG[env['host']]

    with cd(env.settings['sources_folder']):
        sudo('%(interpreter)s pip install -r requirements.txt' % env.settings, user='django')
