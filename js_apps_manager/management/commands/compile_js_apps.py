# -*- coding: utf8 -*-
import itertools
import os
from django.core.management import BaseCommand
from django.conf import settings
import shutil


class Command(BaseCommand):
    def handle(self, *args, **options):
        for app_config in settings.JS_APPS['apps'].itervalues():
            static_root = settings.STATICFILES_DIRS[0]
            app_root = os.path.join(static_root, app_config['app_root'])
            build_dir = app_root + '_build'

            if os.path.isdir(build_dir):
                shutil.rmtree(build_dir)
            os.mkdir(build_dir)

            for libfile in app_config['libs']:
                libpath = os.path.join(static_root, settings.JS_APPS['libs_root'], libfile)
                target_libpath = os.path.join(build_dir, '-min.'.join(libfile.rsplit('.', 1)))
                os.popen('uglifyjs %s -c -m >> %s' % (libpath, target_libpath))

            app_module_path = os.path.join(build_dir, 'app.js')
            with open(app_module_path, 'w') as app_root_module_min:
                for module in app_config['app_modules']:
                    with open(os.path.join(app_root, module), 'r') as app_root_module:
                        app_root_module_min.write(app_root_module.read())

            os.popen('uglifyjs %s -c -m > %s' % (app_module_path, '-min.'.join(app_module_path.rsplit('.', 1))))
            os.remove(app_module_path)
