# -*- coding: utf8 -*-
from django import template

from django.conf import settings
import os

register = template.Library()


@register.inclusion_tag('include_js_app.html')
def include_js_app(app_name):
    app = settings.JS_APPS['apps'][app_name]
    templates = [{'path': os.path.join(app['templates_root'], t),
                  'name': t.split('.')[0] + '-template'}
                 for t in os.listdir(os.path.join(settings.BASE_DIR,
                                                  settings.TEMPLATES[0]['DIRS'][0],
                                                  app['templates_root']))]

    scripts = []

    if settings.DEBUG:
        for lib in app['libs']:
            scripts.append(os.path.join(settings.JS_APPS['libs_root'], lib))
        for module in app['app_modules']:
            scripts.append(os.path.join(app['app_root'], module))
    else:
        for lib in app['libs'] + ['app.js']:
            l = lib.rsplit('.', 1)[0] + '-min.js'
            scripts.append(os.path.join(app['app_root'] + '_build', l))

    return {'templates': templates, 'scripts': scripts}
