"use strict";
var DashboardApp = new Marionette.Application();

// region utility functions
DashboardApp.navigate = function (route, options) {
    options || (options = {});
    Backbone.history.navigate(route, options);
};

DashboardApp.getCurrentRoute = function () {
    return Backbone.history.fragment;
};
// endregion

DashboardApp.addRegions({
    mainRegion: '#main',
    dialogRegion: '#dialog',
    alertRegion: '#alert'
});

//region application event handlers
DashboardApp.on('start', function (options) {
    this.initial = options.initial;

    DashboardApp.trigger('ws:connect');

    new Marionette.AppRouter({
        appRoutes: {
            '': 'showMain',
            'view/:monitorId': 'viewMonitor',
            'profile': 'profile'
        },
        controller: DashboardApp.Controller
    });
    Backbone.history.start();

    if (this.getCurrentRoute() === '') {
        DashboardApp.trigger('page:index');
    }
});

DashboardApp.on('ws:connect', function(){
    DashboardApp.ws = new DashboardApp.WS.Websockets({
        ws_path: this.initial.ws_path,
        ws_auth_token: this.initial.ws_auth_token
    });
});

DashboardApp.on('navigate:monitorList', function () {
    DashboardApp.navigate('');
    DashboardApp.Controller.showMain();
});

DashboardApp.on('navigate:newMonitor', function () {
    DashboardApp.Controller.newMonitor();
});

DashboardApp.on('navigate:viewMonitor', function (monitorId) {
    DashboardApp.Controller.viewMonitor(monitorId);

    if (monitorId.id) {
        monitorId = monitorId.id;
    }

    DashboardApp.navigate('view/' + monitorId);
});

DashboardApp.on('navigate:editMonitor', function (monitorId) {
    DashboardApp.Controller.editMonitor(monitorId);
});

DashboardApp.on('navigate:deleteMonitor', function (monitor) {
    DashboardApp.Controller.deleteMonitor(monitor);
});

DashboardApp.on('ws:common:alert', function (data) {
    DashboardApp.Controller.showAlert(data.type, data.value);
});


//endregion

DashboardApp.getRegion('dialogRegion').onShow = function (view) {
    var self = this;

    var closeDialog = function () {
        self.stopListening();
        self.empty();
        self.$el.dialog("destroy");
    };

    this.$el.dialog({
        modal: true,
        width: 'auto',
        title: view.title || '',
        close: function (e, ui) {
            closeDialog();
        }
    });
};
