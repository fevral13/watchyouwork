"use strict";
DashboardApp.module("Controller", function (Controller, DashboardApp, Backbone, Marionette, $, _) {
    Controller.showMain = function () {
        var loading = new DashboardApp.Views.LoadingView();
        loading.show();

        var fetchingMonitors = DashboardApp.request('models:monitors'),
            fetchingMonitorEvents = DashboardApp.request('models:monitorEvents'),
            fetchingMonitorStatuses = DashboardApp.request('models:monitorStatuses'),
            self = this;

        $.when(fetchingMonitors, fetchingMonitorEvents, fetchingMonitorStatuses).done(function (monitors, monitorEvents, monitorStatuses) {
            loading.destroy();

            var mainLayoutView = new DashboardApp.Views.MainLayoutView({
                monitors: monitors,
                monitorEvents: monitorEvents,
                statuses: monitorStatuses,
                filters: DashboardApp.monitorFilters
            });

            monitorEvents.listenTo(DashboardApp, 'ws:event:new', function (eventData) {
                if (monitorEvents.state.currentPage === 1) {
                    monitorEvents.add(new DashboardApp.Models.MonitorEvent(eventData));
                    if (monitorEvents.length >= 10) monitorEvents.reset(monitorEvents.slice(0, -1));
                    monitorEvents.sort();
                }
            });

            self.listenTo(mainLayoutView, 'childview:monitorfilter:changed', function (sender, filterValue) {
                DashboardApp.monitorFilters.set({status: filterValue});
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitors.getPage(1);
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(mainLayoutView, 'childview:monitor:new', function () {
                Controller.newMonitor();
            });

            self.listenTo(mainLayoutView, 'childview:monitor:page:previous', function () {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitors.getPreviousPage();
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(mainLayoutView, 'childview:monitor:page:next', function () {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitors.getNextPage();
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(mainLayoutView, 'childview:monitor:page', function (sender, page) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitors.getPage(page);
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(mainLayoutView, 'childview:event:page:previous', function () {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitorEvents.getPreviousPage();
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(mainLayoutView, 'childview:event:page:next', function () {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitorEvents.getNextPage();
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(mainLayoutView, 'childview:event:page', function (sender, page) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitorEvents.getPage(page);
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(mainLayoutView, 'childview:childview:monitor:duplicate', function (sender1, sender2, monitor) {
                Controller.newMonitor(monitor);
            });

            self.listenTo(mainLayoutView, 'childview:childview:monitor:open', function (sender1, sender2, monitor) {
                DashboardApp.trigger('navigate:viewMonitor', monitor);
            });

            self.listenTo(mainLayoutView, 'childview:childview:monitor:pause', function (sender1, sender2, monitor) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    pausing = monitor.pause();
                $.when(pausing).done(function () {
                    loading.destroy();
                });
            });
            self.listenTo(mainLayoutView, 'childview:childview:monitor:resume', function (sender1, sender2, monitor) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    resuming = monitor.resume();
                $.when(resuming).done(function () {
                    loading.destroy();
                });
            });

            DashboardApp.getRegion('mainRegion').show(mainLayoutView);
        });
    };


    Controller.newMonitor = function (monitor) {
        var newMonitor = new DashboardApp.Models.Monitor();
        if (monitor) {
            newMonitor.set(_.omit(monitor.attributes, 'id'));
        }

        var monitorFormView = new DashboardApp.Views.NewMonitorForm({
            model: newMonitor,
            prefill: true
        });
        var region = DashboardApp.getRegion('dialogRegion');
        region.show(monitorFormView);

        this.listenTo(monitorFormView, 'close', function () {
            DashboardApp.getRegion('dialogRegion').$el.dialog('close');
        });

        this.listenTo(monitorFormView, 'saveFormData', function (data) {
            var savingMonitor = newMonitor.save(data);
            $.when(savingMonitor).done(function () {
                monitorFormView.trigger('close');
                DashboardApp.trigger('models:newMonitor', newMonitor);
            }).fail(function (response) {
                if (response.status === 422) {
                    monitorFormView.triggerMethod('form:data:invalid', response.responseJSON);
                }
            });
        });
    };


    Controller.editMonitor = function (monitorId) {
        var monitorFetching = DashboardApp.request('models:monitor', monitorId),
            self = this;
        $.when(monitorFetching).done(function (monitor) {
            var monitorFormView = new DashboardApp.Views.EditMonitorForm({
                model: monitor
            });

            var region = DashboardApp.getRegion('dialogRegion');
            region.show(monitorFormView);


            self.listenTo(monitorFormView, 'close', function () {
                DashboardApp.getRegion('dialogRegion').$el.dialog('close');
            });

            self.listenTo(monitorFormView, 'saveFormData', function (data) {
                var savingMonitor = monitor.save(data);
                $.when(savingMonitor).done(function () {
                    monitorFormView.trigger('close');
                }).fail(function (response) {
                    if (response.status === 422) {
                        monitorFormView.triggerMethod('form:data:invalid', response.responseJSON);
                    }
                });
            });

        });
    };


    Controller.newContact = function () {
        var newContact = new DashboardApp.Models.Contact(),
            contactFormView = new DashboardApp.Views.NewContactForm({
                model: newContact
            });
        DashboardApp.getRegion('dialogRegion').show(contactFormView);

        this.listenTo(contactFormView, 'close', function () {
            DashboardApp.getRegion('dialogRegion').$el.dialog('close');
        });

        this.listenTo(contactFormView, 'saveFormData', function (data) {
            var savingContact = newContact.save(data);
            $.when(savingContact).done(function () {
                contactFormView.trigger('close');
                DashboardApp.trigger('models:newContact', newContact);
            }).fail(function (response) {
                if (response.status === 422) {
                    contactFormView.triggerMethod('form:data:invalid', response.responseJSON);
                }
            });
        });
    };


    Controller.editContact = function (contact) {
        var contactFormView = new DashboardApp.Views.EditContactForm({
            model: contact
        });
        DashboardApp.getRegion('dialogRegion').show(contactFormView);

        this.listenTo(contactFormView, 'close', function () {
            DashboardApp.getRegion('dialogRegion').$el.dialog('close');
        });

        this.listenTo(contactFormView, 'saveFormData', function (data) {
            var savingContact = contact.save(data);
            $.when(savingContact).done(function () {
                contactFormView.trigger('close');

            }).fail(function (response) {
                if (response.status === 422) {
                    contactFormView.triggerMethod('form:data:invalid', response.responseJSON);
                }
            });
        });
    };


    Controller.profile = function () {
        var loading = new DashboardApp.Views.LoadingView();
        loading.show();

        var fetchingProfile = DashboardApp.request('models:profile'),
            fetchingTimezones = DashboardApp.request('models:timezones'),
            fetchingContacts = DashboardApp.request('models:contacts'),
            self = this;
        $.when(fetchingProfile, fetchingTimezones, fetchingContacts).done(function (profile,
                                                                                    timezones,
                                                                                    contacts) {
            loading.destroy();
            var profileLayout = new DashboardApp.Views.ProfileLayout({
                profile: profile,
                timezones: timezones[0],
                contacts: contacts
            });
            DashboardApp.getRegion('mainRegion').show(profileLayout);

            self.listenTo(contacts, 'change', function () {
                var activeAndValidContacts = contacts.where({active: true, verified: true});
                if (!activeAndValidContacts.length) {
                    Controller.showAlert('alert-warning', 'You don\'t have any active verified contact. No notofication will be sent');
                }
            });
            contacts.trigger('change');

            self.listenTo(profileLayout, 'childview:profile:changed', function (sender, data) {
                var savingProfile = profile.save(data);
                $.when(savingProfile).done(function () {
                    Controller.showAlert('alert-success', 'Timezone changed');
                });
            });

            self.listenTo(profileLayout, 'childview:saveFormData', function (sender, data) {
                $.ajax({
                    url: '/api/change_password/',
                    context: this,
                    data: JSON.stringify({password: data.password}),
                    method: 'put',
                    contentType: 'application/json',
                    success: function () {
                        Controller.showAlert('alert-success', 'Password changed');
                    },
                    statusCode: {
                        422: function (response) {
                            console.log(response);
                        }
                    }
                });
            });

            self.listenTo(profileLayout, 'childview:contact:verify:phone', function (sender, data) {
                $.ajax({
                    url: '/api/verify_phone/',
                    context: this,
                    data: JSON.stringify(data),
                    method: 'put',
                    contentType: 'application/json',
                    success: function (response) {
                        console.log(response)

                    },
                    statusCode: {
                        400: function (response) {
                            sender.triggerMethod('form:data:invalid', response.responseJSON);
                            console.log(response);
                        }
                    }
                });
            });

            self.listenTo(profileLayout, 'childview:contact:resendVerification', function (sender, data) {
                $.ajax({
                    url: '/api/init_verify_phone/',
                    context: this,
                    data: JSON.stringify(data),
                    method: 'put',
                    contentType: 'application/json',
                    success: function (response) {
                        console.log(response)

                    },
                    statusCode: {
                        400: function (response) {
                            sender.triggerMethod('form:data:invalid', response.responseJSON);
                            console.log(response);
                        }
                    }
                });
            });

            self.listenTo(profileLayout, 'childview:contact:new', function () {
                Controller.newContact();
            });

            self.listenTo(profileLayout, 'childview:childview:contact:edit', function (sender1, sender2, contact) {
                Controller.editContact(contact);
            });

            self.listenTo(profileLayout, 'childview:contact:delete', function (sender, contact) {
                return contact.destroy();
            });

            self.listenTo(profileLayout, 'childview:contact:activate', function (sender, contact) {
                return contact.activate();
            });

            self.listenTo(profileLayout, 'childview:contact:deactivate', function (sender, contact) {
                return contact.deactivate();
            });

            self.listenTo(DashboardApp, 'models:newContact', function (newContact) {
                contacts.add(newContact);
                contacts.sort();
            });

            self.listenTo(DashboardApp, 'ws:contact:verified', function (data) {
                var contact = contacts.get(data.id);
                if (contact) {
                    contact.set(data);
                }
            });

        });
    };


    Controller.deleteMonitor = function (monitor) {
        return monitor.destroy();
    };


    Controller.viewMonitor = function (monitorId) {
        var loading = new DashboardApp.Views.LoadingView().show();

        var mId = monitorId.id ? monitorId.id : monitorId,
            fetchingMonitor = DashboardApp.request('models:monitor', monitorId),
            fetchingMonitorEvents = DashboardApp.request('models:monitorEventsForMonitor', mId),
            fetchingData = DashboardApp.request('models:monitorData', mId),
            self = this;

        $.when(fetchingMonitor, fetchingMonitorEvents, fetchingData).done(function (monitor, monitorEvents, monitorData) {
            loading.destroy();
            var monitorDetailsLayout = new DashboardApp.Views.MonitorDetailsLayout({
                model: monitor,
                monitorEvents: monitorEvents,
                data: monitorData[0]
            });

            monitorEvents.listenTo(DashboardApp, 'ws:event:new', function (eventData) {
                if (eventData.monitor_id === monitor.id && monitorEvents.state.currentPage === 1) {
                    monitorEvents.add(new DashboardApp.Models.MonitorEvent(eventData));
                    if (monitorEvents.length >= 10) monitorEvents.reset(monitorEvents.slice(0, -1));
                    monitorEvents.sort();
                }
            });

            self.listenTo(monitorDetailsLayout, 'childview:event:page:previous', function () {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitorEvents.getPreviousPage();
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });
            self.listenTo(monitorDetailsLayout, 'childview:event:page:next', function () {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitorEvents.getNextPage();
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });
            self.listenTo(monitorDetailsLayout, 'childview:event:page', function (sender, page) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    paging = monitorEvents.getPage(page);
                $.when(paging).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(monitorDetailsLayout, 'childview:monitor:edit', function (sender, monitor) {
                DashboardApp.trigger('navigate:editMonitor', monitor);
            });

            self.listenTo(monitorDetailsLayout, 'childview:monitor:pause', function (sender, monitor) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    pausing = monitor.pause();
                $.when(pausing).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(monitorDetailsLayout, 'childview:monitor:resume', function (sender, monitor) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    resuming = monitor.resume();
                $.when(resuming).done(function () {
                    loading.destroy();
                });
            });

            self.listenTo(monitorDetailsLayout, 'childview:monitor:delete', function (sender, monitor) {
                var loading = new DashboardApp.Views.LoadingView().show(),
                    deletingMonitor = Controller.deleteMonitor(monitor);
                $.when(deletingMonitor).done(function () {
                    loading.destroy();
                    DashboardApp.trigger('navigate:monitorList');
                });
            });

            self.listenTo(monitorDetailsLayout, 'childview:wantRefreshData', function (sender) {
                var fetchingData = DashboardApp.request('models:monitorData', mId);
                $.when(fetchingData).done(function(data){
                    monitorDetailsLayout.triggerMethod('dataChanged', data);
                });

            });

            DashboardApp.getRegion('mainRegion').show(monitorDetailsLayout);
        });
    };


    Controller.showAlert = function (alertType, text) {
        var alert = new DashboardApp.Views.AlertView({
            alertType: alertType,
            text: text
        });
        DashboardApp.getRegion('alertRegion').show(alert);
    };
});
