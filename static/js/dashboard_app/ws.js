"use strict";
DashboardApp.module("WS", function (WS, DashboardApp, Backbone, Marionette, $, _) {
    WS.Websockets = Marionette.Object.extend({
        initialize: function (options) {
            var ws = new WebSocket(options.ws_path);

            ws.sendMsg = function(type, data){
                this.send(JSON.stringify({'type': type, 'data': data}));
            };

            ws.onopen = function () {
                console.log('ws opened');
                this.sendMsg('auth', options.ws_auth_token);
            };

            ws.onmessage = function (msg) {
                var message = JSON.parse(msg.data);

                if (message.type === 'event'){
                    console.log(message.data.channel, message.data.data);
                    DashboardApp.trigger('ws:' + message.data.channel, message.data.data);
                }
            };

            ws.onclose = function(){
                console.log('ws closed');
                setTimeout(function(){
                    DashboardApp.trigger('ws:connect');
                }, 2000);
            };

            this.ws = ws;
        }
    });
});
