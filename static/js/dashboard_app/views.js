"use strict";
DashboardApp.module("Views", function (Views, DashboardApp, Backbone, Marionette, $, _) {


    var required = 'Необходимое поле',
        EditFormMixin = {
            removeErrorLabels: function () {
                this.$el.find('.help-inline.error').remove();
                this.$el.find('.error').removeClass('error');
            },
            onFormDataInvalid: function (errors) {
                _.each(errors, function (value, key) {
                    var $controlGroup = this.$el.find("[name=" + key + ']').parent();
                    if (!_.isArray(value)) {
                        value = [value];
                    }

                    _.each(value, function (val) {
                        var $errorEl = $("<span>", {class: "help-inline error", text: val});
                        $controlGroup.append($errorEl).addClass("error");
                    });

                }, this);

                if (errors.__all__) {
                    var $errorEl = $("<span>", {class: "help-inline error", text: errors.__all__}).insertBefore(
                        this.$el.find('.form-group:first')
                    );

                }
            },
            formSubmit: function (e) {
                e.preventDefault();
                this.removeErrorLabels();
                var data = Backbone.Syphon.serialize(this),
                    errors = this.validate(data);
                if (!_.isEmpty(errors)) {
                    this.triggerMethod('form:data:invalid', errors);
                } else {
                    this.triggerMethod('saveFormData', data)
                }
            }
        };

    Views.MainLayoutView = Marionette.LayoutView.extend({
        className: 'row',
        template: '#main-layout-template',
        regions: {
            monitorsRegion: '#js-monitors',
            eventsRegion: '#js-events'
        },

        onShow: function () {
            var listView = new DashboardApp.Views.MonitorListView({
                    collection: this.options.monitors,
                    statuses: this.options.statuses,
                    filters: this.options.filters
                }),
                monitorEventsView = new DashboardApp.Views.MonitorEventListView({
                    collection: this.options.monitorEvents
                });

            this.showChildView('monitorsRegion', listView);
            this.showChildView('eventsRegion', monitorEventsView);
        }
    });


    Views.MonitorDetailsLayout = Marionette.LayoutView.extend({
        template: '#monitor-details-layout-template',
        regions: {
            detailsRegion: '#js-monitor-details',
            plotRegion: '#js-monitor-plot',
            eventsRegion: '#js-monitor-events'
        },

        onShow: function () {
            var detailsView = new Views.MonitorDetailsView({
                model: this.options.model
            });
            var eventsView = new Views.MonitorEventListView({
                collection: this.options.monitorEvents,
                itemTemplate: '#monitor-event-list-item-short-template'
            });
            var plotView = new Views.MonitorPlotView({
                data: this.options.data,
                dataStatus: this.options.dataStatus
            });
            this.showChildView('detailsRegion', detailsView);
            this.showChildView('eventsRegion', eventsView);
            this.showChildView('plotRegion', plotView);
            this.plotView = plotView;
        },
        serializeData: function () {
            var data = _.clone(this.options.model.attributes);
            data.urlDisplay = trimUrl(data.url, 80);

            return data;
        },

        onDataChanged: function (attrs) {
            this.plotView.triggerMethod('dataChanged', attrs);
        }
    });


    Views.ProfileLayout = Marionette.LayoutView.extend({
        template: '#profile-layout-template',
        className: 'row',
        regions: {
            profileRegion: '#js-profile-form',
            contactsRegion: '#js-contacts'
        },

        onShow: function () {
            var profileFormView = new Views.ProfileFormView({
                    model: this.options.profile,
                    timezones: this.options.timezones
                }),
                contactListView = new Views.ContactListView({
                    collection: this.options.contacts
                });
            this.showChildView('profileRegion', profileFormView);
            this.showChildView('contactsRegion', contactListView);
        }
    });


    Views.ProfileFormView = Marionette.ItemView.extend({
        template: '#profile-form-template',
        events: {
            'change select[name=timezone]': 'changeProfile',
            'click .js-change-password': 'formSubmit'
        },
        serializeData: function () {
            var data = _.clone(this.options.model.attributes);
            data.timezones = this.options.timezones;
            return data;
        },
        onShow: function () {
            Backbone.Syphon.deserialize(this, this.serializeData());
        },
        changeProfile: function () {
            var data = Backbone.Syphon.serialize(this);
            this.triggerMethod('profile:changed', data);
        },
        validate: function (data) {
            var errors = {};
            if (!data.password) {
                errors.password = required;
            }
            return errors;
        }
    });
    _.extend(Views.ProfileFormView.prototype, EditFormMixin);


    Views.ContactListItemView = Marionette.ItemView.extend({
        template: '#contact-list-item-template',
        tagName: 'tr',
        events: {
            'click .js-delete': 'deleteContact',
            'click .js-edit': 'editContact',
            'click .js-verify': 'verifyPhone',
            'click .js-resend': 'resend',
            'click .js-activate': 'activate',
            'click .js-deactivate': 'deactivate'
        },
        editContact: function (e) {
            e.preventDefault();
            this.triggerMethod('contact:edit', this.model);
        },
        modelEvents: {
            'change': 'render',
            'sync': 'render'
        },

        deleteContact: function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (confirm('Delete this contact?')) {
                this.triggerMethod('contact:delete', this.model);
            }
        },

        activate: function (e) {
            e.preventDefault();
            this.triggerMethod('contact:activate', this.model);
        },

        deactivate: function (e) {
            e.preventDefault();
            this.triggerMethod('contact:deactivate', this.model);
        },

        verifyPhone: function (e) {
            e.preventDefault();
            this.removeErrorLabels();
            var data = Backbone.Syphon.serialize(this);
            if (data.code) {
                data.contact_id = this.model.id;
                this.triggerMethod('contact:verify:phone', data);
            }
        },

        resend: function (e) {
            e.preventDefault();
            this.removeErrorLabels();
            var data = {};
            data.contact_id = this.model.id;
            this.triggerMethod('contact:resendVerification', data);
        }
    });
    _.extend(Views.ContactListItemView.prototype, EditFormMixin);


    Views.ContactListView = Marionette.CompositeView.extend({
        childView: Views.ContactListItemView,
        tagName: 'table',
        childViewContainer: 'tbody',
        template: '#contact-list-template',
        className: 'table table-bordered',
        events: {
            'click .js-new': 'newContact'
        },
        newContact: function (e) {
            e.preventDefault();
            this.triggerMethod('contact:new');
        }
    });


    Views.NewContactForm = Marionette.ItemView.extend({
        title: 'New contact',
        action: 'Create',
        tagName: 'form',
        className: 'form',
        template: '#contact-form-template',

        serializeData: function () {
            var data = _.clone(this.model.attributes);
            data.action = this.action;
            return data;
        },
        events: {
            'click .js-save': 'formSubmit',
            'click .js-cancel': 'cancel'
        },

        cancel: function (e) {
            e.preventDefault();
            this.trigger('close');
        },

        validate: function (data) {
            var errors = {};

            _.each(['contact_type', 'value'], function (attr) {
                if (!data[attr]) {
                    errors[attr] = required;
                }
            });

            return errors;
        }

    });
    _.extend(Views.NewContactForm.prototype, EditFormMixin);


    Views.EditContactForm = Views.NewContactForm.extend({
        title: 'Edit contact',
        action: 'Save',
        onShow: function () {
            Backbone.Syphon.deserialize(this, this.serializeData());
        }
    });


    var serializePaginatedCollection = function (collection) {
        var data = _.clone(collection.state);
        data.hasNextPage = collection.hasNextPage();
        data.hasPreviousPage = collection.hasPreviousPage();
        return data;
    };


    var getLabelClass = function (status) {
        if (status == 2) {
            return 'label-success';
        } else if (status == 4) {
            return 'label-danger';
        } else if (status == 1) {
            return 'label-primary';
        } else {
            return 'label-default';
        }
    };


    var trimUrl = function (url, trimTo) {
        var _trimTo = trimTo || 50;

        if (url.length > _trimTo) {
            return url.slice(0, _trimTo) + '...';
        } else {
            return url;
        }
    };


    Views.MonitorDetailsView = Marionette.ItemView.extend({
        template: '#monitor-details-template',
        className: 'col-md-12',
        modelEvents: {
            'change': 'render',
            'sync': 'render'
        },
        events: {
            'click .js-edit': 'editMonitor',
            'click .js-pause': 'pauseMonitor',
            'click .js-resume': 'resumeMonitor',
            'click .js-delete': 'deleteMonitor'
        },
        pauseMonitor: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:pause', this.model);
        },
        resumeMonitor: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:resume', this.model);
        },
        editMonitor: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:edit', this.model);
        },
        deleteMonitor: function (e) {
            e.preventDefault();
            if (confirm('Delete this monitor?')) {
                this.triggerMethod('monitor:delete', this.model);
            }
        },
        serializeData: function () {
            var data = _.clone(this.model.attributes);
            data.labelClass = getLabelClass(data.status);
            data.urlDisplay = trimUrl(data.url, 100);

            return data;
        }
    });


    Views.MonitorListItemView = Marionette.ItemView.extend({
        template: '#monitor-list-item-template',
        tagName: 'li',
        className: 'list-group-item cursor-pointer',

        modelEvents: {
            'change': 'render'
            //'sync': 'render'
        },

        events: {
            'click': 'openMonitor',
            'click .js-edit': 'editMonitor',
            'click .js-pause': 'pauseMonitor',
            'click .js-resume': 'resumeMonitor',
            'click .js-delete': 'deleteMonitor',
            'click .js-duplicate': 'duplicateMonitor'
        },

        onRender: function () {
            this.scheduler = setInterval(this.refreshMonitorData.bind(this), 1000 * 60);
            this.refreshMonitorData();
        },

        onBeforeDestroy: function () {
            clearInterval(this.scheduler);
        },

        refreshMonitorData: function () {
            var fetchingMonitorData = DashboardApp.request('models:monitorData', this.model.id, 1, 1),
                self = this;
            $.when(fetchingMonitorData).done(function (data) {
                self.paintCharts(data);
            });
        },

        paintCharts: function (data) {
            var $responseChart = this.$el.find('.js-response-chart'),
                $statusChart = this.$el.find('.js-status-chart'),
                responseChartWidthTotal = $responseChart.width(),
                responseChartDataLength = data.length,
                max = d3.max(data, function (i) {
                    return i.duration;
                }),
                min = d3.min(data, function (i) {
                    return i.duration;
                }),
                yScale = d3.scale.linear()
                    .domain([min, max * 1.3])
                    .range([0, 80]),
                xScale = d3.scale.ordinal()
                    .domain(d3.range(responseChartDataLength))
                    .rangeBands([50, responseChartWidthTotal]),

                yAxisScale = d3.scale.linear()
                    .domain([max * 1.3, min])
                    .range([0, 80]),
                yAxis = d3.svg.axis().scale(yAxisScale).orient('left').ticks(3).tickFormat(function (val) {
                    return val + ' ms'
                });


            var responsesSvg = d3.select($responseChart[0]),
                responses = responsesSvg.attr('height', 80)
                    .selectAll('rect')
                    .data(data);

            responsesSvg.select('g').remove();
            responsesSvg.append('g').attr('class', 'axis').attr('transform', 'translate(49, 20)').call(yAxis);

            function applyResponseChartAttributes(_responses) {
                _responses.attr('class', 'responseChartBar')
                    .attr('x', function (d, i) {
                        return xScale(i);
                    })
                    .attr('width', function (d) {
                        return xScale.rangeBand();
                    })
                    .attr('y', function (d) {
                        return 80 - yScale(d.duration);
                    })
                    .attr('height', function (d) {
                        return yScale(d.duration);
                    });
            }

            applyResponseChartAttributes(responses.enter().append('rect'));
            applyResponseChartAttributes(responses);
            responses.exit().remove();

            var statusesSvg = d3.select($statusChart[0]),
                statuses = statusesSvg.attr('height', 50)
                    .selectAll('rect')
                    .data(data);

            var xAxisScale = d3.scale.ordinal()
                    .domain(data.map(function (d) {
                        return (new Date(d.date)).toLocaleTimeString();
                    }))
                    .rangeBands([0, responseChartWidthTotal - 50]),
                xAxis = d3.svg.axis()
                    .scale(xAxisScale)
                    .tickFormat(function (d, i) {
                        if (!(i % 15)) {
                            return d;
                        }
                    });
            statusesSvg.select('g').remove();
            statusesSvg.append('g').attr('class', 'axis').attr('transform', 'translate(49, 20)').call(xAxis);

            function applyStatusChartAttributes(_statuses) {
                _statuses
                    .attr('width', function (d) {
                        return xScale.rangeBand();
                    })
                    .attr('x', function (d, i) {
                        return xScale(i);
                    })
                    .attr('class', function (d) {
                        return 'status-' + d.status;
                    })
                    .attr('height', 20)
            }

            applyStatusChartAttributes(statuses.enter().append('rect'));
            applyStatusChartAttributes(statuses);
            statuses.exit().remove();
        },

        openMonitor: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:open', this.model);
        },

        editMonitor: function (e) {
            e.preventDefault();
            e.stopPropagation();
            DashboardApp.trigger('navigate:editMonitor', this.model);
        },

        duplicateMonitor: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.triggerMethod('monitor:duplicate', this.model);
        },

        deleteMonitor: function (e) {
            e.preventDefault();
            e.stopPropagation();
            if (confirm('Delete this monitor?')) {
                DashboardApp.trigger('navigate:deleteMonitor', this.model);
            }
        },

        pauseMonitor: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.triggerMethod('monitor:pause', this.model);
        },

        resumeMonitor: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.triggerMethod('monitor:resume', this.model);
        },

        serializeData: function () {
            var data = _.clone(this.model.attributes);
            data.labelClass = getLabelClass(data.status);
            data.urlDisplay = trimUrl(data.url);

            return data;
        }
    });

    Views.MonitorFilterPanelView = Marionette.ItemView.extend({
        template: '#monitor-filter-panel-template',
        events: {
            'click .js-filter': 'applyFilter'
        },

        initialize: function(options){
            this.listenTo(options.filters, 'change', this.render.bind(this));
            this.listenTo(options.statuses, 'change', this.render.bind(this));
            this.listenTo(options.statuses, 'fetch', this.render.bind(this));
        },

        serializeData: function () {
            var data = {};
            data.up = this.options.statuses.attributes['2'];
            data.down = this.options.statuses.attributes['4'];
            data.paused = this.options.statuses.attributes['3'];
            data.all = this.options.statuses.all();
            data.status = this.options.filters.attributes.status;
            return data;
        },

        applyFilter: function (e) {
            e.preventDefault();
            var filterValue = $(e.target).data('filter');
            if (filterValue === undefined) {
                filterValue = $(e.target).parent().data('filter');
            }
            this.triggerMethod('monitorfilter:changed', filterValue);
        }

    });

    Views.MonitorListView = Marionette.CompositeView.extend({
        childView: Views.MonitorListItemView,
        childViewContainer: '.js-monitors',
        template: '#monitor-list-template',
        className: 'panel panel-default',
        events: {
            'click .js-new-monitor': 'newMonitor',
            'click .js-prev-page': 'goPrevPage',
            'click .js-next-page': 'goNextPage',
            'click .js-go-page': 'goPage'
        },

        onShow: function(){
            var filtersView = new Views.MonitorFilterPanelView({
                statuses: this.options.statuses,
                filters: this.options.filters
            });
            this.listenTo(filtersView, 'monitorfilter:changed', function(args){
                this.triggerMethod('monitorfilter:changed', args);
            });
            this.$el.find('.js-filter-region').html(filtersView.render().$el);
        },

        newMonitor: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:new');
        },

        serializeData: function () {
            return serializePaginatedCollection(this.options.collection);
        },

        goPrevPage: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:page:previous');
        },

        goNextPage: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:page:next');
        },

        goPage: function (e) {
            e.preventDefault();
            var page = parseInt($(e.target).data('page'), 10);
            this.triggerMethod('monitor:page', page);
        }
    });


    Views.MonitorEventListItemView = Marionette.ItemView.extend({
        template: '#monitor-event-list-item-template',
        tagName: 'tr',
        serializeData: function () {
            var data = _.clone(this.model.attributes);
            data.labelClass = getLabelClass(data.type_id);
            data.urlDisplay = trimUrl(data.monitor.url);

            return data;
        },
        events: {
            'click .js-open-monitor': 'openMonitor'
        },
        openMonitor: function (e) {
            e.preventDefault();
            this.triggerMethod('monitor:open', this.model.get('monitor').id);
        }
    });


    Views.MonitorEventListView = Marionette.CompositeView.extend({
        childView: Views.MonitorEventListItemView,
        childViewContainer: '.js-monitor-events',
        template: '#monitor-event-list-template',
        className: 'panel panel-default',
        events: {
            'click .js-prev-page': 'goPrevPage',
            'click .js-next-page': 'goNextPage',
            'click .js-go-page': 'goPage'
        },
        initialize: function (options) {
            this.listenTo(options.collection, 'sync', this.render.bind(this));
            this.listenTo(options.collection, 'sort', this.render.bind(this));

            if (options.itemTemplate) {
                this.childViewOptions = function () {
                    return {
                        template: options.itemTemplate
                    }
                }
            }
        },

        serializeData: function () {
            return serializePaginatedCollection(this.collection);
        },

        goPrevPage: function (e) {
            e.preventDefault();
            this.triggerMethod('event:page:previous');
        },

        goNextPage: function (e) {
            e.preventDefault();
            this.triggerMethod('event:page:next');
        },

        goPage: function (e) {
            e.preventDefault();
            var page = parseInt($(e.target).data('page'), 10);
            this.triggerMethod('event:page', page);
        }

    });

    Views.MonitorPlotView = Marionette.ItemView.extend({
        template: '#monitor-plot-template',
        initialize: function (options) {
            var self = this;
            this.scheduler = setInterval(function () {
                self.triggerMethod('wantRefreshData');
            }, 1000 * 60);  // 60 seconds
        },
        onBeforeDestroy: function () {
            clearInterval(this.scheduler);
        },
        onShow: function () {
            this.paintCharts();
        },
        onDataChanged: function (data) {
            this.paintCharts(data);
        },
        paintCharts: function (_data) {
            var data = _data || this.options.data,

                chartWidth = this.$el.find('#js-response-chart').width() - 40,
                dataLength = data.length,

                min = d3.min(data, function (i) {
                    return i.duration;
                }),
                max = d3.max(data, function (i) {
                    return i.duration;
                }),

                yScale = d3.scale.linear()
                    .domain([min, max * 1.1])
                    .range([0, 200]),
                xScale = d3.scale.ordinal()
                    .domain(d3.range(dataLength))
                    .rangeBands([40, chartWidth]),

                yAxisScale = d3.scale.linear()
                    .domain([max * 1.1, min])
                    .range([0, 240]),
                yAxis = d3.svg.axis().scale(yAxisScale).orient('left').ticks(5).tickFormat(function (val) {
                    return val + ' ms';
                }),


                xAxisScale = d3.scale.ordinal()
                    .domain(data.map(function (d) {
                        return (new Date(d.date)).toLocaleTimeString();
                    }))
                    .rangeBands([0, chartWidth - 40]),
                xAxis = d3.svg.axis()
                    .scale(xAxisScale)
                    .tickFormat(function (d, i) {
                        if (!(i % 20)) {
                            return d;
                        }
                    });

            var svg = d3.select('#js-response-chart'),
                responses = svg
                    .selectAll('rect')
                    .data(data);

            svg.select('g').remove();
            svg.append('g').attr('class', 'axis').attr('transform', 'translate(39, 21)').call(yAxis);


            function applyResponseChartAttributes(_responses) {
                _responses.attr('class', 'responseChartBar')
                    .attr('x', function (d, i) {
                        return xScale(i);
                    })
                    .attr('width', function (d) {
                        return xScale.rangeBand();
                    })
                    .attr('y', function (d) {
                        return 220 - yScale(d.duration);
                    })
                    .attr('height', function (d) {
                        return yScale(d.duration);
                    });
            }

            applyResponseChartAttributes(responses.enter().append('rect'));
            applyResponseChartAttributes(responses);
            responses.exit().remove();

            var statusesSvg = d3.select('#js-status-bar'),
                statuses = statusesSvg
                    .selectAll('rect')
                    .data(data);

            statusesSvg.select('g').remove();
            statusesSvg.append('g').attr('transform', 'translate(39, 21)').attr('class', 'axis').call(xAxis);

            function applyStatusChartAttributes(_statuses) {
                _statuses
                    .attr('width', function (d) {
                        return xScale.rangeBand();
                    })
                    .attr('x', function (d, i) {
                        return xScale(i);
                    })
                    .attr('class', function (d) {
                        return 'status-' + d.status;
                    })
                    .attr('height', 20)
            }

            applyStatusChartAttributes(statuses.enter().append('rect'));
            applyStatusChartAttributes(statuses);
            statuses.exit().remove();

        }

    });


    Views.NewMonitorForm = Marionette.ItemView.extend({
        title: 'New monitor',
        action: 'Create',
        tagName: 'form',
        className: 'form',
        template: '#monitor-form-template',

        serializeData: function () {
            var data = _.clone(this.model.attributes);
            data.monitorTypes = DashboardApp.initial.monitorTypes;
            data.intervalSchedules = DashboardApp.initial.intervalSchedules;
            data.regions = DashboardApp.initial.regions;
            data.action = this.action;
            return data;
        },
        events: {
            'click .js-save': 'formSubmit',
            'click .js-cancel': 'cancel',
            'click .js-get-region': 'getRegion',
            'change #monitorType': 'monitorTypeChanged'
        },

        getRegion: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this),
                guessingRegion = DashboardApp.request('models:getRegion', data.url),
                self = this;
            $.when(guessingRegion).done(function (response) {
                self.$el.find('select[name=region] option[value=' + response.region + ']').attr('selected', true);
                if (response.guess_ok) {
                    DashboardApp.Controller.showAlert('alert-success', 'Region auto-detected "' + response.region_name + '"');
                } else {
                    DashboardApp.Controller.showAlert('alert-warning', 'Region auto-detect failed. Chosen default "' + response.region_name + '"');
                }
            });

        },

        monitorTypeChanged: function (e) {
            var data = Backbone.Syphon.serialize(this),
                type = data.type,
                $extraFields = this.$el.find('#extra-fields'),
                templates = {
                    '1': '#monitor-form-ping-template',
                    '2': '#monitor-form-http-template'
                },
                templateName = templates[type];

            if (templateName) {
                $extraFields.html(_.template($(templateName).html(), {}));
            } else {
                $extraFields.empty();
            }
            Backbone.Syphon.deserialize(this, data);
        },

        cancel: function (e) {
            e.preventDefault();
            this.trigger('close');
        },

        onShow: function () {
            if (this.options.prefill) {
                Backbone.Syphon.deserialize(this, this.model.attributes);
                this.monitorTypeChanged();
                Backbone.Syphon.deserialize(this, this.serializeData());
            }
        },

        validate: function (data) {
            var errors = {};


            _.each(['url', 'interval_schedule', 'region', 'type'], function (attr) {
                if (!data[attr]) {
                    errors[attr] = required;
                }
            });

            if (data.type === '2') { //if type==HTTP
                if (!data.verb) {
                    errors.verb = required;
                }
                if (data.keyword && (!data.should)) {
                    errors.should = required;
                }

                if (data.password && (!data.username)) {
                    errors.username = required;
                }
            }

            return errors;
        }


    });
    _.extend(Views.NewMonitorForm.prototype, EditFormMixin);


    Views.EditMonitorForm = Views.NewMonitorForm.extend({
        title: 'Change monitor',
        action: 'Save',
        onShow: function () {
            Backbone.Syphon.deserialize(this, this.serializeData());
            this.monitorTypeChanged();
            Backbone.Syphon.deserialize(this, this.serializeData());
        }
    });


    Views.AlertView = Marionette.ItemView.extend({
        template: '#alert-template',
        className: function () {
            return 'alert alert-dismissable ' + this.options.alertType || 'alert-success';
        },

        onShow: function () {
            var self = this;
            setTimeout(function () {
                self.destroy();
            }, 5000);
        },

        serializeData: function () {
            return {text: this.options.text || ''};
        }
    });


    Views.LoadingView = Marionette.ItemView.extend({
        template: '#loading-template',
        className: 'alert alert-info loading',

        show: function () {
            var left = Math.round(($(document).width() - 150) / 2);
            this.$el.css('left', left + 'px');
            $('body').append(this.render().el);
            return this;
        }
    });
});
