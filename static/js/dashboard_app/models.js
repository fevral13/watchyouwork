"use strict";
DashboardApp.module("Models", function (Models, DashboardApp, Backbone, Marionette, $, _) {

    var monitorBaseUrl = '/api/monitor/',
        monitorStatusesBaseUrl = '/api/monitor_statuses/',
        profileBaseUrl = '/api/profile/',
        contactBaseUrl = '/api/contact/',
        monitorEventBaseUrl = '/api/monitor_events/';
    DashboardApp.monitors = null;
    DashboardApp.monitorEvents = null;
    DashboardApp.monitorStatuses = null;


    //region base models
    // base class for all models
    Models.BaseModel = Backbone.Model.extend({
        url: function () {
            return this.baseUrl + (this.id ? this.id + '/' : '');
        }
    });

    // base class for all collections
    Models.BaseCollection = Backbone.PageableCollection.extend({
        state: {
            firstPage: 1,
            pageSize: 10,
            totalRecords: 100000
        },
        parseRecords: function (data) {
            return data.objects;
        },
        parseState: function (data) {
            return {
                totalRecords: data.totalRecords,
                currentPage: data.page
            };
        }
    });
    //endregion

    //region monitors
    Models.Monitor = Models.BaseModel.extend({
        baseUrl: monitorBaseUrl,
        defaults: {
            type: '',
            running: '',
            region: '',
            interval_schedule: '',
            status: '',
            name: '',
            url: ''
        },


        pause: function () {
            return this.save({running: false});
        },

        resume: function () {
            return this.save({running: true});
        }
    });

    Models.Monitors = Models.BaseCollection.extend({
        //url: monitorBaseUrl,
        url: function () {
            return monitorBaseUrl + '?' + DashboardApp.monitorFilters.serialize();
        },
        model: Models.Monitor,

        initialize: function () {
            this.listenTo(DashboardApp, 'ws:monitor:changed', function (newData) {
                var updatedMonitor = this.get(newData.id);
                if (updatedMonitor){
                    updatedMonitor.set(newData);
                }

            }, this);
        }
    });

    DashboardApp.reqres.setHandler('models:monitors', function () {
        var defer = $.Deferred();

        if (DashboardApp.monitors) {
            defer.resolve(DashboardApp.monitors);
        } else {
            DashboardApp.monitors = new Models.Monitors();

            DashboardApp.monitors.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
        }

        return defer.promise();
    });

    DashboardApp.reqres.setHandler('models:monitor', function (monitorId) {
        var defer = $.Deferred();

        if (_.isNumber(monitorId) || _.isString(monitorId)) {
            var monitor = new Models.Monitor({
                id: monitorId
            });

            monitor.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });

        } else {
            defer.resolve(monitorId);
        }

        return defer.promise();
    });

    DashboardApp.on('models:newMonitor', function (monitor) {
        DashboardApp.monitors.add(monitor);
    });
    //endregion

    // region monitor events
    Models.MonitorEvent = Models.BaseModel.extend({
        saseUrl: monitorEventBaseUrl,
        defaults: {}
    });

    Models.MonitorEvents = Models.BaseCollection.extend({
        model: Models.MonitorEvent,

        initialize: function (options) {
            if (options)
                this.monitorId = options.monitorId;
        },
        url: function () {
            if (this.monitorId) {
                return monitorEventBaseUrl + '?monitor_id=' + this.monitorId;
            } else {
                return monitorEventBaseUrl;
            }
        },
        comparator: function (model) {
            return -model.id;
        }

    });

    DashboardApp.reqres.setHandler('models:monitorEvents', function () {
        var defer = $.Deferred();

        if (DashboardApp.monitorEvents) {
            defer.resolve(DashboardApp.monitorEvents);
        } else {
            DashboardApp.monitorEvents = new Models.MonitorEvents();

            DashboardApp.monitorEvents.fetch({
                success: function (data) {
                    defer.resolve(data);
                }
            });
        }
        return defer.promise();
    });

    DashboardApp.reqres.setHandler('models:monitorEventsForMonitor', function (monitorId) {
        var defer = $.Deferred(),
            events = new Models.MonitorEvents({monitorId: monitorId});

        events.fetch({
            success: function (data) {
                defer.resolve(data)
            }
        });

        return defer.promise();
    });

    // endregion

    // region monitor statuses
    Models.MonitorStatuses = Backbone.Model.extend({
        url: monitorStatusesBaseUrl,
        defaults: {
            '1': 0,
            '2': 0,
            '3': 0,
            '4': 0
        },
        all: function () {
            return _.reduce(_.values(this.attributes), function (a, b) {
                return a + b
            }, 0);
        },
        initialize: function () {
            var self = this;
            this.listenTo(DashboardApp, 'ws:monitor:changed', this.onMonitorChange, this);
            this.listenTo(DashboardApp, 'ws:event:new', this.onMonitorChange, this);
        },

        onMonitorChange: function(){
            this.fetch();
        }
    });

    DashboardApp.reqres.setHandler('models:monitorStatuses', function () {
        var defer = $.Deferred();

        if (!DashboardApp.monitorStatuses) {
            DashboardApp.monitorStatuses = new Models.MonitorStatuses();
        }
        DashboardApp.monitorStatuses.fetch({
            success: function (data) {
                defer.resolve(data);
            }
        });
        return defer.promise();
    });
    // endregion


    //region filters
    Models.MonitorFilters = Backbone.Model.extend({
        defaults: {
            status: 0
        },
        serialize: function () {
            var items = [];
            _.each(this.attributes, function (value, key) {
                items.push(key + '=' + value);
            });

            return items.join('&');
        }
    });

    DashboardApp.monitorFilters = new Models.MonitorFilters();
    //endregion


    DashboardApp.reqres.setHandler('models:monitorData', function (monitorId, _interval, _duration) {
        var interval = _interval || 5,
            duration = _duration || 24,
            url = '/api/monitor_data/' + monitorId + '/' + '?interval=' + interval + '&duration=' + duration;
        return $.getJSON(url, {});
    });

    //region profile
    Models.Profile = Backbone.Model.extend({
        url: profileBaseUrl,
        defaults: {
            timezone: ''
        }
    });

    DashboardApp.reqres.setHandler('models:profile', function () {
        var defer = $.Deferred();

        var profile = new Models.Profile({});

        profile.fetch({
            success: function (data) {
                defer.resolve(data);
            }
        });

        return defer.promise();
    });
    //endregion

    DashboardApp.reqres.setHandler('models:timezones', function () {
        return $.getJSON('/api/timezones/', {});
    });

    Models.Contact = Models.BaseModel.extend({
        baseUrl: contactBaseUrl,
        defaults: {
            contact_type: '',
            value: '',
            active: '',
            verified: ''
        },

        activate: function(){
            return this.save({active: true});
        },

        deactivate: function(){
            return this.save({active: false});
        }
    });

    Models.Contacts = Backbone.Collection.extend({
        url: contactBaseUrl,
        model: Models.Contact,
        comparator: 'id'
    });

    DashboardApp.reqres.setHandler('models:contacts', function () {
        var defer = $.Deferred();

        var contacts = new Models.Contacts();

        contacts.fetch({
            success: function (data) {
                defer.resolve(data);
            }
        });

        return defer.promise();
    });

    DashboardApp.reqres.setHandler('models:getRegion', function (url) {
        return $.ajax({
            url: '/api/region/',
            context: this,
            data: {url: url},
            method: 'get',
            contentType: 'application/json'
        });
    });


});
