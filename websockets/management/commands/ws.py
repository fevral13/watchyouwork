# -*- coding:utf-8 -*-
from datetime import datetime
import logging

from django.core.management import BaseCommand
import tornado

from tornado.web import Application
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop, PeriodicCallback
from websockets.handlers.events import EventsHandler
from websockets.handlers.ok import OkHandler
from django.conf import settings

logger = logging.getLogger('ws')


def heart_beat():
    logger.debug('Heartbeat %s' % datetime.now())


class Command(BaseCommand):
    help = 'Runs tornado websockets server on given port'

    def handle(self, port=8001, *args, **options):
        application = tornado.web.Application([
            (r'/ws', EventsHandler),
            (r'/ok', OkHandler),
        ])

        server = HTTPServer(application)
        server.listen(port)

        logger.info('Serving on %s' % port)
        instance = IOLoop.instance()

        if settings.WS_HEARTBEAT:
            PeriodicCallback(heart_beat, settings.WS_HEARTBEAT).start()

        instance.start()
