# -*- coding:utf-8 -*-
import socket
import time
import urllib2
import subprocess
import logging

from django.core.management import BaseCommand
from django.conf import settings


logger = logging.getLogger('ws')


def restart_instance(job_name):
    subprocess.call(['supervisorctl', 'restart', '%s' % job_name])


def run_check(args):
    port, job_name = args
    url = u'http://127.0.0.1:{port}/ok'.format(port=port, protocol=settings.PROTOCOL)
    try:
        response = urllib2.urlopen(url, timeout=5)
    except (urllib2.URLError, socket.timeout):
        logger.warning([url, 'unavaliable'])
        restart_instance(job_name)
    else:
        code = response.getcode()
        if code != 200:
            logger.warning([url, 'wrong code %s' % code])
            restart_instance(job_name)
        else:
            logger.info([url, 'ok'])


class Command(BaseCommand):
    help = 'Runs tornado websockets health monitor'

    def handle(self, *args, **options):
        if not settings.WS_HEALTH_MON_JOBS:
            logger.warning('Joblist is empty')
            return
        while True:
            map(run_check, settings.WS_HEALTH_MON_JOBS)
            time.sleep(settings.WS_HEALTH_MON_DELAY)
