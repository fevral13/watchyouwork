# -*- coding:utf-8 -*-
from __future__ import print_function
from json import loads
import logging
import tornado.httpserver
import tornado.web
import tornado.websocket
import tornado.ioloop
import tornado.gen

import tornadoredis
from django.conf import settings
from main.views.base_classes import dumps
from websockets.helpers import validate_websockets_auth_token

logger = logging.getLogger(__name__)


class EventsHandler(tornado.websocket.WebSocketHandler):
    def get_channel(self):
        return 'user_%s' % self.user_id

    @tornado.gen.engine
    def listen(self, user_id):
        self.user_id = user_id
        self.client = tornadoredis.Client(host=settings.REDIS_HOST,
                                          selected_db=settings.REDIS_DB,
                                          password=settings.REDIS_PASSWORD)
        self.client.connect()

        yield tornado.gen.Task(self.client.subscribe, self.get_channel())
        self.client.listen(self.on_event)

    def on_event(self, message, *args, **kwargs):
        if message.kind == 'message':
            logger.info(message.body)
            self.send('event', loads(message.body))

    def open(self):
        logger.info('%s connected to %s' % (self.request.remote_ip, self))
        self.send('ok', 'hi!, authenticate, please')

    def check_origin(self, origin):
        return True

    def on_message(self, msg):  # msg from web client
        message = loads(msg)
        logger.info('%s messaged %s' % (self.request.remote_ip, message))
        if message['type'] == 'auth':
            self.user_id = validate_websockets_auth_token(message['data'])
            if self.user_id:
                self.listen(self.user_id)
                self.send('auth', u'auth ok')
                logger.info('%s auth ok' % (self.request.remote_ip, ))
            else:
                self.send('auth', u'auth failed')

    def on_close(self):
        logger.info('%s disconnected' % self.request.remote_ip)
        try:
            if self.client.subscribed:
                yield tornado.gen.Task(self.client.unsubscribe, self.get_channel())
            self.client.disconnect()
        except AttributeError:
            pass

    def send(self, type, data):
        self.write_message(dumps({'type': type, 'data': data}))
