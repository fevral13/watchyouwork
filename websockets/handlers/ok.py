# -*- coding:utf-8 -*-
from tornado.web import RequestHandler


class OkHandler(RequestHandler):
    def get(self, *args, **kwargs):
        self.write('ok')