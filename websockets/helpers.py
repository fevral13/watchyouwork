# -*- coding:utf-8 -*-
from datetime import datetime
import hashlib
from django.conf import settings


def create_websockets_auth_token(user_id):
    salt = hashlib.md5(str(datetime.now())).hexdigest()
    s = '%s%s%s' % (user_id, settings.SECRET_KEY, salt)

    return '%s:%s:%s' % (user_id, hashlib.sha224(s).hexdigest(), salt)


def validate_websockets_auth_token(token):
    user_id, checksum, salt = token.split(':')
    if checksum == hashlib.sha224('%s%s%s' % (user_id, settings.SECRET_KEY, salt)).hexdigest():
        return user_id
    return None

